<?php
namespace frontend\controllers\v1;

use backend\models\Article;
use backend\models\ArticleClassify;
use backend\models\ArticleImg;
use Yii;
use common\models\LoginForm;
use backend\models\ArticleRate;


/**
 * Site controller
 */
class WebsiteController extends \deepziyu\yii\rest\Controller
{
    public $responseType = 'json';
    public static $code = [
        '200'   => '成功',
        '400'   => '参数错误',
        '401'   =>  '数据错误',
    ];

    /***
     * @return 网站首页
     */
    public function actionIndex()
    {
        $arr = [];
        $area = Yii::$app->request->post('area', 0);
        $arr['rate'] = ArticleRate::find()->where(['pid' => $area])->select('id,name,rate')->asArray()->limit(10)->all();
        $i= 0;
        foreach($arr['rate'] as &$v)
        {
            $i ++;
            $v['id'] = $i;
        }
        unset($v);
        $lunbo = ArticleImg::find()->where(['type' => 1])->select('id,img_url,alt')->asArray()->all();
        $arr['service_img'] = ArticleImg::find()->where(['type' => 3])->select('id,img_url,alt')->asArray()->one();
        $arr['service_img']['img_url'] = Yii::$app->params['img_url'].$arr['service_img']['img_url'];

        $arr['feature'] = ArticleImg::find()->where(['type' => 2])->select('id,img_url,alt')->limit(4)->asArray()->all();
        foreach($arr['feature'] as &$v)
        {
            $v['img_url'] = Yii::$app->params['img_url'].$v['img_url'];
        }
        unset($v);
        foreach($lunbo as &$v)
        {
            $v['img_url'] = Yii::$app->params['img_url'].$v['img_url'];
        }
        unset($v);
        $arr['lunbo'] = $lunbo;
        // 购房贷款
        $article = Article::find()->where(['pid' => 1, 'status' => 2, 'type' => 1, 'position' => 1])->select('id,title,keywords,head_img,content')->orderBy('publish_time desc')->asArray()->limit(9)->all();
        foreach($article as &$v)
        {
            $v['head_img'] = Yii::$app->params['img_url'].$v['head_img'];
            $v['content'] = strip_tags($v['content']);
        }
        unset($v);
        $arr['article'] = $article;
        // 贷款攻略
        $classify = ArticleClassify::find()->where(['status' => 1])->limit(5)->all();
        foreach($classify as $k => $v)
        {
            $arr['stra'][$k]['key'] = $arr['product'][$k]['key'] = $v->name;
            $arr['stra'][$k]['content'] = Article::find()->where(['pid' => $v->id, 'status' => 2, 'type' => 1, 'position' => 1])->select('id,title,keywords')->orderBy('publish_time desc')->asArray()->limit(7)->all();
            $arr['product'][$k]['content'] = Article::find()->where(['pid' => $v->id, 'status' => 2, 'type' => 2, 'position' => 0])->select('id,title,keywords')->orderBy('publish_time desc')->asArray()->limit(7)->all();
        }

        return $arr;
    }

    /***
     * @return 侧边栏文章列表
     */
    public function actionArticleList()
    {
        $arr = [];
        $arr['article1'] = Article::find()->where(['status' => 2, 'type' => 1, 'position' => 2])->select('id,title,keywords')->orderBy('publish_time desc')->asArray()->limit(11)->all();
        $i= 0;
        foreach($arr['article1'] as &$v)
        {
            $i ++;
            $v['key'] = $i;
        }
        unset($v);
        $arr['article2'] = Article::find()->where(['status' => 2, 'type' => 1, 'position' => 3])->select('id,title,keywords')->orderBy('publish_time desc')->asArray()->limit(11)->all();
        $i= 0;
        foreach($arr['article2'] as &$v)
        {
            $i ++;
            $v['key'] = $i;
        }
        unset($v);
        $arr['product'] = Article::find()->where(['status' => 2, 'type' => 2, 'position' => 2])->select('id,company_logo,company_logo_alt,product_logo,product_logo_alt,custom1_img,custom1_alt,custom2_img,custom2_alt,custom3_img,custom3_alt,rate,rate_label,money,time_limit,is_mortgage,desc_mortgage,desc,content,keywords')->orderBy('publish_time desc')->asArray()->orderBy('publish_time desc')->limit(4)->all();
        foreach($arr['product'] as &$v)
        {
            $v['company_logo'] = !empty($v['company_logo']) ? Yii::$app->params['img_url'].$v['company_logo'] : '';
            $v['product_logo'] = !empty($v['product_logo']) ? Yii::$app->params['img_url'].$v['product_logo'] : '';
            $v['custom1_img'] = !empty($v['custom1_img']) ? Yii::$app->params['img_url'].$v['custom1_img'] : '';
            $v['custom2_img'] = !empty($v['custom2_img']) ? Yii::$app->params['img_url'].$v['custom2_img'] : '';
            $v['custom3_img'] = !empty($v['custom3_img']) ? Yii::$app->params['img_url'].$v['custom3_img'] : '';
            $v['content'] = strip_tags($v['content']);
        }
        return $arr;
    }

    /***
     * @return 产品首页
     */

    public function actionProduct()
    {
        $arr = [];
        $cover_img = ArticleImg::find()->where(['type' => 4])->one();
        $arr['cover_img'] = Yii::$app->params['img_url'] . $cover_img['img_url'];
        $arr['p'] = $p = Yii::$app->request->post('p', 1);
        $pageSize = 10;
        $count = Article::find()->where(['status' => 2, 'type' => 2])->count();
        $arr['page'] = ceil($count / $pageSize);
        $arr['product'] = Article::find()
            ->where(['status' => 2, 'type' => 2])
            ->select('id,title,company_logo,company_logo_alt,product_logo,product_logo_alt,custom1_img,custom1_alt,custom2_img,custom2_alt,custom3_img,custom3_alt,rate,rate_label,money,time_limit,is_mortgage,desc_mortgage,content')
            ->offset(($p - 1) * $pageSize)
            ->limit($pageSize)
            ->orderBy('publish_time desc')->all();

        foreach($arr['product'] as &$v)
        {
            $v['company_logo'] = !empty($v['company_logo']) ? Yii::$app->params['img_url'].$v['company_logo'] : '';
            $v['product_logo'] = !empty($v['product_logo']) ? Yii::$app->params['img_url'].$v['product_logo'] : '';
            $v['custom1_img'] = !empty($v['custom1_img']) ? Yii::$app->params['img_url'].$v['custom1_img'] : '';
            $v['custom2_img'] = !empty($v['custom2_img']) ? Yii::$app->params['img_url'].$v['custom2_img'] : '';
            $v['custom3_img'] = !empty($v['custom3_img']) ? Yii::$app->params['img_url'].$v['custom3_img'] : '';
        }

        return $arr;
    }

    /***
     * 产品详情页
     */
    public function actionProductDetail()
    {
        $id = Yii::$app->request->post('id', '');
        if(!$id)
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '参数不正确';
            return '';
        }
        $arr = [];
        $cover_img = ArticleImg::find()->where(['type' => 4])->one();
        $arr['cover_img'] = Yii::$app->params['img_url'] . $cover_img['img_url'];
        $arr['product'] = Article::find()->where(['id' => $id])->select('title,keywords,desc,company_logo,company_logo_alt,product_logo,product_logo_alt,custom1_img,custom1_alt,custom2_img,custom2_alt,custom3_img,custom3_alt,rate,rate_label,money,time_limit,is_mortgage,desc_mortgage,content,type')->one();
        if(!$arr['product'])
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '产品不存在';
            return '';
        }
        if($arr['product']['type'] == 1)
        {
            Yii::$app->response->statusCode=400;
            return '';
        }
        $arr['product']['company_logo'] = !empty($arr['product']['company_logo']) ? Yii::$app->params['img_url'] . $arr['product']['company_logo'] : '';
        $arr['product']['product_logo'] = !empty($arr['product']['product_logo']) ? Yii::$app->params['img_url'] . $arr['product']['product_logo'] : '';
        $arr['product']['custom1_img'] = !empty($arr['product']['custom1_img']) ? Yii::$app->params['img_url'] . $arr['product']['custom1_img'] : '';
        $arr['product']['custom2_img'] = !empty($arr['product']['custom2_img']) ? Yii::$app->params['img_url'] . $arr['product']['custom2_img'] : '';
        $arr['product']['custom3_img'] = !empty($arr['product']['custom3_img']) ? Yii::$app->params['img_url'] . $arr['product']['custom3_img'] : '';


        return $arr;
    }

    /***
     * 文章列表页
     */
    public function actionArticle()
    {
        $arr = [];
        $cover_img = ArticleImg::find()->where(['type' => 5])->one();
        $arr['cover_img'] = Yii::$app->params['img_url'] . $cover_img['img_url'];
        $arr['p'] = $p = Yii::$app->request->post('p', 1);
        $pageSize = 10;
        $count = Article::find()->where(['status' => 2, 'type' => 1])->count();
        $arr['page'] = ceil($count / $pageSize);
        $arr['article'] = Article::find()
            ->where(['status' => 2, 'type' => 1])
            ->select('id,pid,head_img,head_img_alt,title,keywords,content,publish_time')
            ->offset(($p - 1) * $pageSize)
            ->limit($pageSize)
            ->asArray()
            ->orderBy('publish_time desc')->all();

        foreach($arr['article'] as $k => &$v)
        {
            $v['head_img'] = !empty($v['head_img']) ? Yii::$app->params['img_url'].$v['head_img'] : '';
            $classify = ArticleClassify::find()->where(['id' => $v['pid']])->one();
            $v['classify'] = mb_substr($classify->name, 0, 2);
            $v['content'] = strip_tags($v['content']);
        }

        return $arr;
    }

    /***
     * 文章详情页
     */
    public function actionArticleDetail()
    {
        $arr = [];
        $cover_img = ArticleImg::find()->where(['type' => 5])->one();
        $arr['cover_img'] = Yii::$app->params['img_url'] . $cover_img['img_url'];
        $id = $p = Yii::$app->request->post('id', '');
        if(!$id)
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '参数不正确';
            return '';
        }
        $ids = Article::find()->where("status = 2 and type = 1 and id < $id")->select('id,title')->asArray()->orderBy('id desc')->limit(1)->one();
        $arr['pre_id'] = isset($ids) ? $ids['id'] : '';
        $arr['pre_title'] = isset($ids) ? $ids['title'] : '';
        $ids = Article::find()->where("status = 2 and type = 1 and id > $id")->select('id,title')->asArray()->orderBy('id asc')->limit(1)->one();
        $arr['next_id'] = isset($ids) ? $ids['id'] : '';
        $arr['next_title'] = isset($ids) ? $ids['title'] : '';
        $arr['detail'] = Article::find()->where(['id' => $id])->select('title,head_img,head_img_alt,keywords,content,publish_time,type')->one();
        if(!$arr['detail'])
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '文章不存在';
            return '';
        }
        $arr['detail']['publish_time'] = date('Y-m-d H:i:s');
        $arr['detail']['head_img'] = !empty($arr['detail']['head_img']) ? Yii::$app->params['img_url'].$arr['detail']['head_img'] : '';

        if($arr['detail']['type'] == 2)
        {
            Yii::$app->response->statusCode=400;
            return '';
        }
        unset($arr['type']);
        return $arr;
    }
    /*
     * 页面图
     */
    public function actionImg()
    {
        $type = Yii::$app->request->post('type', '');
        if(!$type || !in_array($type, [6,7,8]))
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '参数不正确';
            return '';
        }
        $arr = ArticleImg::find()->where(['type' => $type])->one();
        $arr['img_url'] = Yii::$app->params['img_url'] . $arr['img_url'];
        return $arr;
    }

}