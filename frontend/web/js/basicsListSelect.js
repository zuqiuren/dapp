
$(function(){	
	
	$(".dowListSelect").change(function(){
		
		var val = $(this).val();
		if(val == 1)
		{
			//定额  
			var html = '<div class="dowListBox1" id="dowlist"><div class="dowList"><div class="ding"><input type="text" name="Basics[quota]" class="monney"/> 元</div></div></div>';
			$("#dowlist").html(html);
		}
		
		if(val == 2)
		{
			//区间随机
			var html = '<div class="dowListBox2" id="dowlist"><div class="dowList"><div class="ding"><input type="text" name="Basics[region_min]" class="monney"/>元-<input type="text" name="Basics[region_max]" class="monney"/>元</div></div></div>';
			$("#dowlist").html(html);

		}
		
		if(val == 3){
			//定额随机
			var html = '<div class="dowListBox3" id="dowlist"><div class="dowList"><select name="Basics[red_quota]" id="dingListSelect"><option value="1">1个</option><option value="2">2个</option><option value="3">3个</option><option value="4">4个</option><option value="5">5个</option><option value="6">6个</option><option value="7">7个</option><option value="8">8个</option><option value="9">9个</option><option value="10">10个</option></select></div><div class="dowList" id="dowList1"><div class="ding"><input type="text" name="Basics[money][]" class="monney"/>元-<input type="text" name="Basics[percent][]" class="monney"/>%</div></div></div>';
			$("#dowlist").html(html); 
		}
		
	});
	
	//定额随机获取
		$("#dingListSelect").live('change',function(){
	  
		   var id = $(this).val();
	   		$.ajax({
				url: "/basics/ajax-basics",
				type:"get",
				async:false,//同步
				data:{
					"id":id
				},
				success: function(data, textStatus){
					$("#dowList1").html(data); //返回到页面
				}
			});
	});
	
});
	