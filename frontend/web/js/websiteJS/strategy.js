var str = location.href;
// alert(str);
// 获取当页面的Id
var arr = str.split("=");
var p = 1;
if(arr[1])
{
	p = parseInt(arr[1]);
}
$(function(){
	$.ajax({
		type:"post",
		url:" /v1/website/article",
		data:{p:p},
		dataType:"json",
		success:function (data) {
			console.log(data);
			if(data.code == 200) {
				var list = data.data;
				var rateList = list.article;
				// alert(rateList.length)
				var strategyStr = '';
				var banner = list.cover_img;
				for(var i =0; i<rateList.length;i++){
					strategyStr += 
					
					 '<div class="strategyItem" onclick="window.open(\'strategyDetail.html?id=' + rateList[i].id +'\')">'
					// + '<span class="rightLogo"><img src="../../img/websiteImg/bankRightLogo.png"/></span>'
					+ '<span class="rightLogo"><span class="rightW">'+ rateList[i].classify +'</span></span>'
					+ 	'<div class="strategyItemLeft">'
					+  	'<img src=" ' + rateList[i].head_img +'" alt=" '+ rateList[i].head_img_alt +' " width="164" height="105"/>'
					+   '</div>'	
					+	'<div class="strategyItemRight">'
					+		'<p class="strategyItemRight1">' + rateList[i].title  +'</p>'
					+		'<p class="strategyItemRight2">日期：'+ rateList[i].publish_time +'</p>'
					+		'<p class="strategyItemRight3">'+ rateList[i].content  +'</p>'
					+	'</div>'
					+   '</div>'
					
				}

				$('.productsListItem').html(strategyStr);
				$('.bannerProduct img').attr('src', banner);
				//alert(typeof(p))
				$("#pagination2").pagination({
					currentPage: p,
					totalPage: parseInt(list.page),
					isShow: false,
					count: 10,
					prevPageText: "< 上一页",
					nextPageText: "下一页 >",
					callback: function(current) {
						$("#current2").text(current)
					}
				});
			}
		},
		error: function (XMLHttpRequest) {
			if( XMLHttpRequest.status == 400)
			{
				location.href = "404.html";
			}
			else
			{
				location.href = "505.html";
			}
		}
		
	})
})