<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    //添加读取配置文件
    //require(__DIR__ . '/../../common/config/check-conf.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'language' => 'zh-CN',
    'id' => 'app-backend',
    'defaultRoute' => 'index',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'backend\controllers',
    'components' => [
        'session' => [
          'timeout' => 7200
        ],
        'request'=>array(
            'enableCsrfValidation'=>(isset($_POST['PHPSESSID'])) ? false  : true,
            //'enableCsrfValidation'=> false,
            'enableCookieValidation'=>(isset($_POST['PHPSESSID'])) ? false  : true,
        ),
        
        
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
//        'mongodb' => [
//            'class' => '\yii\mongodb\Connection',
//            //'dsn' => 'mongodb://localhost:27017/advertisement',
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'error/index',
        ],
        'urlManager' => [
           'enablePrettyUrl' => true,
           'showScriptName' => false,
           'rules'=>[
               '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
           ],
       ],
        'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@ad/messages',
                    'fileMap' => [
                        'frontend/comment' => 'comment.php'
                    ]
                ]
            ]
        ]
    ],
    'params' => $params,
];
