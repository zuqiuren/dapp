<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

class WidgetFile extends ActiveRecord
{

    /**
     * 配置文本信息
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%config_widget_file}}';
    }

    public function rules()
    {
        return [
            [['name', 'widget_name', 'widget_remark'], 'required'],
            [['type','is_more','must','file_num','content','create_time','update_time'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return array(
            'name' => '标题名称',
            'content' => '描述内容',
            'type' => '附件类型(0 不限制 1图片，2 pdf/word)',
            'must' => '是否必填',
            'is_more' => '是否允许多个（0否1是）',
            'file_num' => '上传附件个数',
            'widget_name' => '组件名称',
            'widget_remark' => '组件备注',
        );
    }
}