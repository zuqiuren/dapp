<?php
namespace backend\models;

use Yii;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;
use yii\base\Module;

class Menu extends ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%pro_menu}}';
    }
    
    public function rules() {
        return [
            [['name'],'required'],
            [['is_show'],'required'],
            [['url'],'default','value'=>''],
            [['weight'],'number'],
            [['parent_id'],'default','value'=>0],
            [['name'], 'string', 'max' => 18],
            [['channel'], 'required']
        ];
    }
    
    public function attributeLabels()
    {
        return array(
            'name'      => '名称',
            'mark'      => 'MARK',
            'url'       => 'url路径',
            'parent_id' => '所属分类',
            'weight'    => '排序',
            'is_show'   => "是否显示",
            'channel'   => "所属系统"
        );
    }
    
    /**
     * 返回是否显示
     * @author  li
     * @since   2015-09-23
     */
    public static function display() {
        return array(
            0 => '不显示',
            1 => '显示'
        );
    }
    
    /**
     * 获取模板列表
     * @author  li
     * @since   2015-05-18
     */
     public function moduleList() {
         $ids = [];
         $data = Menu::find()->orderBy('channel desc')->where(['parent_id'=>0,'is_show'=>1])->asArray()->all();
         foreach ($data as $value) {
             $ids[] = $value['id'];
         }
         $find = Menu::find()->where(['parent_id'=>$ids])->asArray()->all();
         
         foreach ($data as $key=>$value) {
             foreach ($find as $val) {
                 if($value['id']==$val['parent_id']) {
                     $data[$key]['find'][] = $val;
                 }
             }
         }
         return $data;
     }
     
     /*
      * 获取父级菜单
      */
     public static function getParent($channel) {
         $mArr = [];
         $data = Menu::find()->where(['parent_id' => 0, 'channel' => $channel])->all();
         $mArr[0] = '一级菜单';
         foreach ($data as $v) {
             $mArr[$v['id']] = $v['name'];
         }
         return $mArr;
     }

    public static function getChannel($id)
    {
        return Menu::find()->where(['id' => $id])->one();
    }
    
}
