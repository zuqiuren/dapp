<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

class WidgetText extends ActiveRecord
{

    /**
     * 配置文本信息
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%config_widget_text}}';
    }

    public function rules()
    {
        return [
            [['name', 'widget_name', 'widget_remark'], 'required'],
            [['content','prompt','must','check','style','create_time','update_time'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return array(
            'name' => '标题名称',
            'content' => '描述内容',
            'prompt' => '默认提示文字',
            'must' => '是否必填',
            'check' => '正则校验',
            'style' => '文本类型',
            'widget_name' => '组件名称',
            'widget_remark' => '组件备注',
        );
    }
}