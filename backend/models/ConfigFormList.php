<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

class ConfigFormList extends ActiveRecord
{

    /**
     * 配置文本信息
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%config_form_list}}';
    }

    public function rules()
    {
        return [
            [['form_id', 'type'], 'required'],
            [['label','desc'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return array(

        );
    }
}