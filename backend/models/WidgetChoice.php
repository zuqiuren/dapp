<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

class WidgetChoice extends ActiveRecord
{

    /**
     * 配置选择信息
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%config_widget_choice}}';
    }

    public function rules()
    {
        return [
            [['name', 'style', 'single_multiple', 'widget_name', 'widget_remark'], 'required'],
            [['content','must','most','least','widget_name','widget_remark','create_time','update_time'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return array(
            'name' => '标题名称',
            'content' => '描述内容',
            'style' => '1下拉2下拉输入3区块选择',
            'single_multiple' => '1单选2多选',
            'must' => '是否必填（0非1必须）',
            'most' => '最多选择几项',
            'least' => '最少选择几项',
            'no_limit_least' => '多选最少选几项（0限制1不限）',
            'other' => '其他选择（0没有选择1选择）',
            'other_content' => '其他选项的内容',
            'content_all' => '选项内容',
            'widget_name' => '组件名称',
            'widget_remark' => '组件备注',
        );
    }
}