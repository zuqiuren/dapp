<?php
namespace backend\models;

use Yii;
use yii\web\IdentityInterface;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
/**
 * Login form
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    
    public $remember = true;
    private $_user = false;
    
    public function behaviors()
    {
        return 
        [
            [
                "class"=> TimestampBehavior::className(),
                "value"=> function() {
                    return date('Y-m-d H:i:s');
                },
            ]
            
        ];
    }
    
    public static function tableName()
    {
        return '{{pro_admins}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * 返回是否显示
     * @author  li
     * @since   2015-09-23
     */
    public static function status() {
        return array(
            0 => '停用',
            10 => '启用'
        );
    }
    
    /**
     * 获取状态
     * @param string $key
     * @return string|multitype:string
     */
    public static function getStatus($key = false)
    {
        $data = [
            0 => '已删除',
            10 => '正常'
        ];
        
        if($key!==false){
            return isset($data[$key])?$data[$key]:'';
        }
        return $data;
    }
    
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }
    
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
    
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    
    /**
     * 取列表带分页
     * @param array $where
     * @param string $order
     * @param int $page
     * @param int $pageSize
     * @return array
     * @author xi
     * @date 2015-4-18
     */
    public static function getList($where =array() ,$order='',$page=1,$pageSize=10)
    {
        $db = self::find();
        if($where){
            $db->where($where);
        }
        //总数
        $totalNum = $db->count();
        
        //当有结果时进行组合数据
        if($totalNum>0)
        {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);
             
            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }
             
            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            if($order!=''){
                $db->orderBy($order);
            }
            $list = $db->asArray()->all();
             
            return array(
                'totalNum'	=> $totalNum,
                'totalPage' => $totalPage,
                'page'		=> $page,
                'list'		=> $list
            );
        }
        else
        {
            return array(
                'totalNum'	=> 0,
                'totalPage' => 0,
                'page'		=> $page,
                'list'		=> array()
            );
        }
    }
    
    public static function changeStatus($id,$status=0)
    {
        $model = self::findOne(['id'=>$id]);
        $model->status = $status;
        return $model->update();
    }
}
