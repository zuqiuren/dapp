<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\ImageThumb;
use common\models\Paging;


class Dapp extends ActiveRecord
{

    /**
     *
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%dapp}}';
    }

    public function rules()
    {
        return [
            [['account_id', 'dapp_name', 'developer', 'category', 'tagline', 'version', 'description', 'website_url', 'status', 'check_remark', 'sort_weight', 'dau', 'create_time', 'onsale_time', 'offsale_time', 'update_time', 'is_valid', 'bonus'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return array(
            'account_id' => '账户id',
            'dapp_name' => 'dapp名字',
            'developer' => '开发者',
            'category' => '分类 0:other, 1:games, 2:exchanges, 3:collectibles, 4:marketplaces, 5:gambling',
            'tagline' => 'tagline',
            'version' => '版本',
            'description' => '描述',
            'website_url' => 'url',
            'status' => '状态 0:提交未审核通过, 1:已审核通过, 2:审核拒绝, 3:下架',
            'check_remark' => '审核备注',
            'sort_weight' => '排序权重',
            'dau' => '日活跃用户数量',
            'create_time' => '创建时间',
            'onsale_time' => '上架时间',
            'offsale_time' => '下架时间',
            'update_time' => '更新时间',
            'is_valid' => 'alt',
            'bonus' => 'alt',
        );
    }

    public static function getList($where =array() ,$order='',$page=1,$pageSize=10)
    {
        $db = self::find();
        if($where){
            $db->where($where);
        }
        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            if($order!=''){
                $db->orderBy($order);
            }
            $list = $db->asArray()->all();

            return array(
                'totalNum'	=> $totalNum,
                'totalPage' => $totalPage,
                'page'		=> $page,
                'list'		=> $list
            );
        }
        else
        {
            return array(
                'totalNum'	=> 0,
                'totalPage' => 0,
                'page'		=> $page,
                'list'		=> array()
            );
        }
    }
}