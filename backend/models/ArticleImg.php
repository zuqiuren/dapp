<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\ImageThumb;

class ArticleImg extends ActiveRecord
{

    /**
     * 配置游戏数据表
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%article_img}}';
    }

    public function rules()
    {
        return [
            [['img_url','alt','type'], 'required'],
            ];
    }
    public function getInfo($type)
    {
        return self::find()->where(['type' => $type, 'status' => 1])->all();
    }
}