<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\ImageThumb;

class Article extends ActiveRecord
{

    /**
     * 配置游戏数据表
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%article_list}}';
    }

    public function rules()
    {
        return [
            [['title','content','pid','status'], 'required'],
            ['rate_label', 'string', 'max'=>4, 'tooLong' => '最多四个字'],
            [['head_img','position','head_img_alt','company_logo','company_logo_alt','product_logo','product_logo_alt','custom1_img','custom1_alt','custom2_img','custom2_alt','custom3_img','custom3_alt','rate_label','rate','money','time_limit','is_mortgage','desc_mortgage','keywords','desc'], 'safe']

        ];
    }

    public function attributeLabels()
    {
        return array(
            'title' => '标题',
            'head_img_alt' => 'alt',
            'keywords' => '关键字',
            'position' => '推荐位置',
            'desc' => '描述',
            'content' => '内容',
            'company_logo' => '企业logo',
            'company_logo_alt' => 'alt',
            'product_logo' => '产品logo',
            'product_logo_alt' => 'alt',
            'is_mortgage' => '有无抵押',
            'desc_mortgage' => '抵押描述',
            'rate' => '利率内容',
            'rate_label' => '利率标签',
            'money' => '可贷金额',
            'time_limit' => '还款期限',
            'custom1_img' => '产品客服',
            'custom1_alt' => 'alt',
            'custom2_alt' => 'alt',
            'custom3_alt' => 'alt',
        );
    }

    public static function getList($where =array() ,$order='',$page=1,$pageSize=10)
    {
        $db = self::find();
        if($where){
            $db->where($where);
        }
        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            if($order!=''){
                $db->orderBy($order);
            }
            $list = $db->asArray()->all();

            return array(
                'totalNum'	=> $totalNum,
                'totalPage' => $totalPage,
                'page'		=> $page,
                'list'		=> $list
            );
        }
        else
        {
            return array(
                'totalNum'	=> 0,
                'totalPage' => 0,
                'page'		=> $page,
                'list'		=> array()
            );
        }
    }
}

