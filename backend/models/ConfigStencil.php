<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

class ConfigStencil extends ActiveRecord
{

    /**
     * 配置文本信息
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%config_stencil}}';
    }

    public function rules()
    {
        return [
            [['type','name','common_info_id','feature_info_id','base_info_id','supplement_info_id','status'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return array(
            'name' => '分类名',
            'status' => '删除状态',
        );
    }
}