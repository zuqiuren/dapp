<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\ImageThumb;

class DappPreview extends ActiveRecord
{

    /**
     *
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%dapp_preview}}';
    }

    public function rules()
    {
        return [
            [['dapp_id', 'logo_image_url', 'screenshot_image_url', 'lunbo_image_url', 'status'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return array(
            'dapp_id' => 'dapp id',
            'logo_image_url' => 'dapp logo',
            'lunbo_image_url' => 'lunbo image',
            'screenshot_image_url' => 'dapp 应用详情图',
            'status' => '状态 0:删除, 1:正常',
        );
    }
}