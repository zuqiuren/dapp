<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\ImageThumb;

class ArticleClassify extends ActiveRecord
{

    /**
     * 配置游戏数据表
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%article_classify}}';
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['pid', 'safe'],
            [['status'],'safe'],
            ['create_time', 'safe'],
            ['author', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return array(
            'name' => '分组名称',
            'mid' => '权限',
            'channel' => '选择系统'
        );
    }

    public static function getList($where =array() ,$order='',$page=1,$pageSize=10)
    {
        $db = self::find();
        if($where){
            $db->where($where);
        }
        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            if($order!=''){
                $db->orderBy($order);
            }
            $list = $db->asArray()->all();

            return array(
                'totalNum'	=> $totalNum,
                'totalPage' => $totalPage,
                'page'		=> $page,
                'list'		=> $list
            );
        }
        else
        {
            return array(
                'totalNum'	=> 0,
                'totalPage' => 0,
                'page'		=> $page,
                'list'		=> array()
            );
        }
    }
}

