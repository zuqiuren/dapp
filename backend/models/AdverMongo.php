<?php

namespace backend\models;

use yii\mongodb\ActiveRecord;
use Yii;
use yii\mongodb\Query;

class AdverMongo extends ActiveRecord
{
    public function __construct(){
      //  $this->_collection_name = 'advertisement' ;
    }
    public static function getDb()
    {
        return Yii::$app->mongodb;
    }

    public static function addAdver($data)
    {
        $collection = Yii::$app->mongodb->getCollection ( 'advertisement' );
        return $collection->insert($data);
    }
    public static function findAdver()
    {
        return self::find()->from('advertisement')->asArray()->all();
    }
    public static function findNewAdver($request_id)
    {
        return self::find()->where(['request_id' => $request_id])->group('request_id')->orderBy('datetime desc')->from('new_advertisement')->asArray()->all();

    }
}