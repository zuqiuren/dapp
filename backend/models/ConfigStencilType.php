<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

class ConfigStencilType extends ActiveRecord
{

    /**
     * 配置文本信息
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%config_stencil_type}}';
    }

    public function rules()
    {
        return [
            [['name','status'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return array(
            'name' => '分类名',
            'status' => '删除状态',
        );
    }
}