<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\ImageThumb;

class ArticleRate extends ActiveRecord
{

    /**
     * 配置游戏数据表
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%article_rate}}';
    }

    public function rules()
    {
        return [
            [['name','rate'], 'required'],
        ];
    }
    public function getInfo($type)
    {
        return self::find()->where(['type' => $type])->all();
    }
}