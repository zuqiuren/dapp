<?php
namespace backend\models;

use backend\models\User;
use yii\base\Model;
use backend\models\Group;
use Yii;

/**
 * Signup form
 */
class UserRegister extends Model
{
    public $username;
    public $email;
    public $password;
    public $gid;
    public $p_id;
    public $c_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\backend\models\User', 'on'=>'add','message' => '该用户已被占用'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            [['gid'],'required'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],

            ['password', 'required','on'=>'add'],
            ['password', 'string', 'min' => 6],
            
            [['gid','p_id','c_id'],'number'],
        ];
    }
    
    public function attributeLabels()
    {
        return array(
            'username' => '用户名',
            'password' => '密码',
            'email'    => '电子邮件',
            'gid'      => '分组',
            'p_id'    => '城市'
        );
    }
    
    /**
     * Signs user up.
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username   = $this->username;
            $user->email      = $this->email;
            $user->gid        = $this->gid;
            // $user->p_id       = $this->p_id;
            // $user->c_id       = $this->c_id;
            $user->status     = 10;
            $user->created_at = time();
            $user->updated_at = time();
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }
        return null;
    }
    
    public function modify($id)
    {
        if ($this->validate()) {
            $user = User::findOne(['id'=>$id]);
            $user->username   = $this->username;
            $user->email      = $this->email;
            $user->gid        = $this->gid;
            // $user->p_id       = $this->p_id;
            // $user->c_id       = $this->c_id;
            $user->status     = 10;
            $user->created_at = time();
            $user->updated_at = time();
            if ($user->save()) {
                return $user;
            }
        }
        return null;
    }

    public function modifyPassword()
    {
        $id = Yii::$app->user->getIdentity()->id;
        $user = User::findOne(['id'=>$id]);
        $user->setPassword($this->password);
        $user->generateAuthKey();
        if ($user->save()) {
            return $user;
        }
    }
    
    /**
     * 获取分组信息
     * @author  li
     * @since   2015-09-18
     */
    public static function getGroup() {
        $group = [];
        $groupArr = Group::find()->asArray()->all();
        foreach ($groupArr as $key => $value) {            
            $group[$value['id']] = $value['name'];
        }
        return $group;
    }
      
    
}
