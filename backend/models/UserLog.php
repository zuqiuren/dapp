<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use backend\models\Admin;

/**
 * User model
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class UserLog extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_log}}';
    }

    public function add($name, $content, $url)
    {
        $userlog = new UserLog();
        $userlog->name = $name;
        $userlog->content  = json_encode($content);
        $userlog->url = $url;
        $userlog->datetime = time();
        $userlog->save();
    }

    public static function getlist($where =array() ,$order='',$page=1,$pageSize=10)
    {
        $db = self::find();
        if($where){
            $db->where($where);
        }
        //总数
        $totalNum = $db->count();

        //当有结果时进行组合数据
        if($totalNum>0)
        {
            //总页数
            $totalPage = ceil($totalNum/$pageSize);

            if($page<1)
            {
                $page = 1;
            }
            else if($page>$totalPage)
            {
                $page = $totalPage;
            }

            $db->offset(($page-1)*$pageSize);
            $db->limit($pageSize);
            if($order!=''){
                $db->orderBy($order);
            }
            $list = $db->asArray()->all();

            return array(
                'totalNum'  => $totalNum,
                'totalPage' => $totalPage,
                'page'      => $page,
                'list'      => $list
            );
        }
        else
        {
            return array(
                'totalNum'  => 0,
                'totalPage' => 0,
                'page'      => $page,
                'list'      => array()
            );
        }
    }
}