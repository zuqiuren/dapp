<?php
namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

class ConfigForm extends ActiveRecord
{

    /**
     * 配置文本信息
     *
     * @return string
     */
    public static function tableName()
    {
        return '{{%config_form}}';
    }

    public function rules()
    {
        return [
            [['operator','rate','least','most','time_limit','mortgage','loan_type','lending_time','status','type','create_time','update_time'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return array(
            'operator' => '操作者',
            'rate' => '年化利率',
            'least' => '最少贷款金额',
            'most' => '最多贷款金额',
            'time_limit' => '还款期限',
            'mortgage' => '0无抵押1有抵押',
            'loan_type' => '贷款类型',
            'lending_time' => '放款时间',
            'type' => '1通用表单，2特色表单，3基础材料表单4，补充材料表单',
            'status' => '0停用1启用',
        );
    }
}