<?php
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
?>
<script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/js/myForm.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/dropify.js"></script>
<link rel="stylesheet" href="/css/dropify.css">

<style>
    #sel,#sel2{
        /*margin:100px;*/
        background: rgba(0,0,0,0);
        width: 249px;
        height: 46px;
        font-family: "微软雅黑";
        font-size: 18px;
        color: black;
        border: 1px #1a1a1a solid;
        border-radius: 5px;
    }
    .modal-dialog { width:500px;height:300px}
    #myModal {height:380px;}
</style>

<ul class="breadcrumb">
    <li><a href="/">首页</a> <span class="divider">/</span></li>
    <li class="active">dapp列表</li>
</ul>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="btn-toolbar">
            <?php
            $form = ActiveForm::begin([
                'id' => 'active-form',
                'action' => '/dapp/list',
                'method' => 'get',
                'options' => [
                    'class' => 'form-signin',
                ],
            ]);

            ?>
            <div class="input-group input-group-lg">
                <input type="text" id="search" placeholder="请输入dapp名字..." value="<?php if($name):?><?= $name?><?php endif;?>" class="form-control" name="app_name">
            </div>
            <div class="col-sm-3">
                <select id="sel" name="category"  class="form-control">
                    <option value="all" <?php if($category == 'all'):?>selected<?php endif;?>>全部分类</option><option value="0" <?php if($category == '0'):?>selected<?php endif;?>>Other</option><option value="1" <?php if($category == 1):?>selected<?php endif;?>>Games</option><option value="2" <?php if($category == 2):?>selected<?php endif;?>>Exchanges</option><option value="3" <?php if($category == 3):?>selected<?php endif;?>>Collectibles</option><option value="5" <?php if($category == 5):?>selected<?php endif;?>>Gambling</option>
                </select>
            </div>
            <div class="col-sm-3">
                <select id="sel2" name="is_mobile"  class="form-control">
                    <option value="all" <?php if($mobile == 'all'):?>selected<?php endif;?>>所有状态</option>
                    <option value="1" <?php if($mobile == '1'):?>selected<?php endif;?>>移动端显示</option>
                    <option value="0" <?php if($mobile == '0'):?>selected<?php endif;?>>非移动端显示</option>
                </select>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

        <div class="well">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>序号</th>
                    <th>logo</th>
                    <th>dapp名称</th>
                    <th>分类</th>
                    <th>作者</th>
                    <th>权重排序</th>
                    <th>设置</th>
                    <th>设置</th>
                    <th>设置</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0;?>
                <?php foreach ($dapp as $k =>$val):?>
                    <tr>
                        <td><?php $i++; echo $i;?></td>
                        <td><img style="width:100px;height:50px" src="<?= $logo[$k]?>"></td>
                        <td><?php echo $val['dapp_name'];?></td>
                        <td><?php if($val['category'] == '0'){echo 'other';}elseif($val['category'] == '1'){echo 'games';}elseif($val['category'] == '2'){echo 'exchanges';}elseif($val['category'] == '3'){echo 'collectibles';}elseif($val['category'] == '4'){echo 'marketplaces';}elseif($val['category'] == '5'){echo 'gambling';}?></td>
                        <td><?php echo $val['developer'];?></td>
                        <td class="input-group input-group-sm"><input type="text" placeholder="0" value="<?= $val['sort_weight']?>" class="form-control tl" id="<?= $val['id'];?>" name="sort_weight"></td>
                        <td>
                            移动端显示<input onclick="is_mobile(<?= $val['id']?>, 'm')" class="mobile" <?php if($val['is_mobile'] == 1 || $val['is_mobile'] == 2):?>checked<?php endif;?> type="checkbox">
                        </td>
                        <td>
                            最近流行<input onclick="is_mobile(<?= $val['id']?>, 'p')" class="popular" <?php if($val['is_mobile'] == 2):?>checked<?php elseif($val['is_mobile'] == 1):?><?php else:?>disabled<?php endif;?> type="checkbox">
                        </td>
                        <td>
                            轮播图<input onclick="is_mobile(<?= $val['id']?>,'l')" <?php if($val['is_mobile'] == 0):?>disabled<?php elseif($val['is_lunbo'] == 1):?>checked<?php endif;?> type="checkbox">

                            <a href="#" data-target="#myModal" data-toggle="modal"  class="btn btn-info btn-lg"><span class="glyphicon glyphicon-picture"></span> Pic</a>
                            <span id="<?=$val['id']?>" data-toggle="<?= $val['dapp_name'];?>" style="display:none"><?= $lunbo[$k]?></span>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>



        <div class="pagination">
            <ul>
                <?= $pageHtml?>
            </ul>
        </div>
        <form action="upload" enctype='multipart/form-data' method="post">
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">上传轮播图</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-10">
                                <input  type="file" name="file" width="100px" class="dropify" id="input-file-events" data-default-file="原图" data-allowed-file-extensions="jpg png jpeg gif"  >
                                <input type="text" name="app_id" id="img_id" style="display:none" value="">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                            <input type="submit" value="提交" class="btn btn-primary">
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal -->
            </div>
        </form>
        <script>
            function is_mobile(id,type)
            {
                $.post('/dapp/alter-status',{'id':id, 'type':type},function(){
                    document.location.reload(true);
                });
                return false;
            }
            $("input[name^='sort_weight']").blur(function(){
                $.post('/dapp/alter-weight',{'id':$(this).attr('id'),'sort':$(this).val()},function(){
                    //    document.location.reload(true);
                });
                return false;
            });
            $('#sel').change(function()
            {
                $('#active-form').submit();
            })
            $('#sel2').change(function()
            {
                $('#active-form').submit();
            })

            $("[data-toggle='modal']").click(function()
            {
                var id = $(this).next().attr('id');
                if($(this).next().html())
                {
                    var img = $(this).next().html();
                    $('.dropify-render').html("<img src='" + img + "'>");
                }
                else
                {
                    $('.dropify-render').html('');
                }
                $('#myModalLabel').html('应用：' + $(this).next().attr('data-toggle') + '，上传轮播图');
                $('#img_id').val(id);
            })
            /**
             *  文件上传js
             * @type {*|jQuery}
             */
            var drEvent = $('.dropify').dropify();
            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("确定删除 \"" + element.file.name + "\" ?");
            });
            drEvent.on('dropify.afterClear', function(event, element) {
                layer.msg('删除成功');
            });

        </script>