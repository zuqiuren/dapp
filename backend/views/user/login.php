<?php
use yii\bootstrap\ActiveForm;
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>TRON后台管理系统</title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" type="text/css"	href="/css/bootstrap.css">

<link rel="stylesheet" type="text/css" href="/css/theme.css">
<link rel="stylesheet" href="/css/font-awesome.css">

<script src="/js/jquery.js" type="text/javascript"></script>
<style type="text/css">
#line-chart {
	height: 300px;
	width: 800px;
	margin: 0px auto;
	margin-top: 1em;
}

.brand {
	font-family: georgia, serif;
}

.brand .first {
	color: #ccc;
	font-style: italic;
}

.brand .second {
	color: #fff;
	font-weight: bold;
}
</style>
</head>
<body>
	<div class="navbar">
		<div class="navbar-inner">
			<ul class="nav pull-right">

			</ul>
			<a class="brand" href="index.html"><span class="first"></span> <span
				class="second">TRON后台管理系统</span></a>
		</div>
	</div>

	<div class="row-fluid">
		<div class="dialog">
			<div class="block">
				<p class="block-heading">登录</p>
				<div class="block-body">
					<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
						<?php echo  $form->field($model, 'username')->input('text',['class'=>'span12']); ?>
						<?php echo  $form->field($model, 'password')->passwordInput( ['class'=>'span12']); ?>
						<input type="submit" class="btn btn-primary pull-right" value="登录" />
						<label class="remember-me">
                        <input type="checkbox" name="UserForm[rememberMe]" value='1'> 记住密码</label>
						<div class="clearfix"></div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
	<script>
	$(function(){
		$('#userform-username').focus();
	});
	</script>
</body>
</html>


