<?php
use yii\bootstrap\ActiveForm;
?>
<link rel="stylesheet" href="/css/font-awesome.css">
<script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/js/myForm.js" type="text/javascript"></script>
<style>
    input[type="radio"]:first-child {margin:-1px !important;}
</style>

<ul class="breadcrumb">
    <li>
        <a href="/">首页</a>
        <span class="divider">/</span>
    </li>
    <li class="active">修改密码</li>
</ul>

<?php
$form = ActiveForm::begin([
    'id' => 'active-form',
    'method' => 'post',
    'options' => [
        'class' => 'form-signin',
        'enctype' => 'multipart/form-data'
    ],
]);

?>
<div class="container-fluid">
    <div class="row-fluid">

        <div class="well">
            <div><?= $form->field($model,'password')->passwordInput(['style'=>'width: 220px;height: 30px;']); ?></div>
            <label class="control-label">再输入一次新密码</label>
            <div class="form-group field-userregister-password">
                <input type="password" name="password_new" id="password" class="form-control" style="width: 220px;height: 30px;">
            </div>
        <div class="btn-toolbar">
            <input type="button" value="修改" class="btn btn-primary">
        </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<script>
    $('.btn-primary').click(function()
    {
        var one = $('#userregister-password').val();
        var two = $('#password').val();
        if(!one || !two || one != two)
        {
            alert('两次输入不相同');
            return false;
        }
        $('#active-form').submit();
    })
    $(function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        $('#message').modal('show');
        $('.error-text span').html('<?php echo Yii::$app->session->getFlash('message');?>');
        <?php endif;?>
    });
</script>