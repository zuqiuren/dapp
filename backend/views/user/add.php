<?php
use yii\bootstrap\ActiveForm;

?>
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
<link rel="stylesheet" href="/css/font-awesome.css">
<script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/js/myForm.js" type="text/javascript"></script>
<style>
    input[type="radio"]:first-child {margin:-1px !important;}                    
</style>

<ul class="breadcrumb">
    <li>
        <a href="/">首页</a>
        <span class="divider">/</span>
    </li>
    <li class="active">添加用户</li>
</ul>

<?php
    $form = ActiveForm::begin([
        'id' => 'active-form',
        'action' => '/user/add',
        'options' => [
            'class' => 'form-signin',
            'enctype' => 'multipart/form-data'
        ],
    ]);

?>
<div class="container-fluid">
    <div class="row-fluid">

        <div class="well">            
            <div id='gid'><?= $form->field($model,'gid')->dropDownList($group,['style'=>'width: 220px;height: 34px;','prompt'=>'请选择角色']); ?></div>
            <div id='username'><?= $form->field($model,'username')->input('text',['style'=>'width: 220px;height: 30px;']); ?></div>
            <div id='email'><?= $form->field($model,'email')->input('text',['style'=>'width: 220px;height: 30px;']); ?></div>
            <div id='password'><?= $form->field($model,'password')->passwordInput(['style'=>'width: 220px;height: 30px;']); ?></div>
        </div>

        <div class="btn-toolbar">
            <button class="btn btn-primary" name="button" value="tj">
                
                <i class="icon-save"></i>添加</button>
            <a href="/user/list" data-toggle="modal" class="btn">取消</a>
            <div class="btn-group"></div>
        </div>
             
    </div>        
</div>
<?php ActiveForm::end(); ?>

<div class="modal small hide fade" id="message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">提示</h3>
    </div>
    <div class="modal-body">
        <p class="error-text"><i class="icon-warning-sign modal-icon"></i><span>删除成功</span></p>
    </div>
    <div class="modal-footer">
         <button class="btn cancel" data-dismiss="modal" aria-hidden="true">确定</button>
    </div>
</div>

<script>
$(function(){
	<?php if(Yii::$app->session->hasFlash('message')):?>
    $('#message').modal('show');
    $('.error-text span').html('<?php echo Yii::$app->session->getFlash('message');?>');
	<?php endif;?>
});
</script>