<?php
use yii\bootstrap\ActiveForm;
use backend\models\Admin;
use backend\models\Group;
?>
<link rel="stylesheet" href="/css/font-awesome.css">
<script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/js/myForm.js" type="text/javascript"></script>

<ul class="breadcrumb">
    <li><a href="/">首页</a> <span class="divider">/</span></li>
    <li class="active">用户列表</li>
</ul>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="btn-toolbar">
            <a href="/user/add"><button class="btn btn-primary" style="margin-bottom:15px;"><i class="icon-plus"></i>新建用户</button></a>
</div>

<div class="well">
    <table class="table table-bordered table-hover definewidth m10">
      <thead>
        <tr>
          <th>#</th>
          <th>用户名</th>
          <th>用户组</th>
          <th>邮箱</th>
          <th>操作</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($user as $val):?>
        <tr <?php if($val==0):?>style="background:#EEEEEE;"<?php endif;?>>
           <?php $g_name = Group::getName($val['gid'])?>
          <td><?php echo $val['id'];?></td>
          <td><?php echo $val['username'];?></td>
          <td><?php echo isset($g_name) ? $g_name : '';?></td>
          <td><?php echo $val['email'];?></td>
          <td>
              <a style="margin-right: 10px;" href="/user/edit?id=<?=$val['id'];?>"><i class="icon-pencil">编辑</i></a>
              <?php if($val['status']==10):?>
                 <a href="/user/delete?id=<?=$val['id'];?>" role="button" class="del" id="<?php echo $val['id'];?>" data-toggle="modal"><i class="icon-remove">停用</i></a>
              <?php elseif($val['status']==0):?>
                <a href="/user/start?id=<?=$val['id'];?>" role="button" class="del" id="<?php echo $val['id'];?>" data-toggle="modal"><i class="icon-remove">启用</i></a>
              <?php endif;?>
          </td>
        </tr>
        <?php endforeach;?>
      </tbody>
    </table>
</div>
<div class="pagination">
    <ul>
        
    </ul>
</div>

<div class="modal small hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">删除确认</h3>
    </div>
    <div class="modal-body">
        <p class="error-text"><i class="icon-warning-sign modal-icon"></i>您真的要删除码？</p>
    </div>
    <div class="modal-footer">
        <button class="btn cancel" data-dismiss="modal" aria-hidden="true">取消</button>
        <button class="btn btn-danger" data-dismiss="modal">确定</button>
    </div>
</div>
<script type="text/javascript">
$(function(){
	   var id = 0;
        $('.btn-danger').click(function(){
            $.getJSON('/user/delete',{'id':id},function(json){
                  if(json.status==1){
              	       document.location.reload(true);
                  }
                  else{
                      alert(json.message);
                  }
             });
            return false;
        });

        $('.del').click(function(){
            id =$(this).attr('id');
        });
});
</script>                
