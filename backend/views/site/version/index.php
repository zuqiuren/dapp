<?php
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use backend\models\User;
?>
<link href="/css/base.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.9.1.min.js"></script>

<style type="text/css">
    body{position: relative;}
</style>


<div class="cou-all">
  <div class="cou-title">
    <b>版本更新管理</b>
        <a href="/version/add" class="cou-return">+ 添加</a>
    </div>
    <div class="neirong_container">
      <div class="neirong_table">
        <table class="cont-tab" style="width:850px;">
          <tr>
              <th>操作系统</th>
              <th>版本号</th>
              <th>是否强制提醒</th>
              <th>版本更新时间</th>
              <th colspan="2">操作</th>
          </tr>
          <?php foreach($model as $value):?>
          <tr>
              <td><?php if($value['type']==1) echo "ios";elseif($value['type']==2) echo "android";?></td>
              <td><?php echo $value['version'];?></td>
              <td>是</td>
              <td><?php echo $value['oper_time'];?></td>
              <td class="opra-col"><a href="/version/edit/?id=<?php echo $value['id']?>">编辑</a></td>
              <td class="opra-col"><a href="/version/del/?id=<?php echo $value['id']?>">删除</a></td>
          </tr>
          <?php endforeach;?>
        </table>
      </div>
    </div>

</body>
<script type="text/javascript">
    var user_id = 0;
    
    // $(".head-show").click(function(){

    //     $(".cover").show();
    //     $(".info-all").show();
    // });
    // 显示头像审核弹出框
    function avatar(id,head_imgurl)
    {
        user_id = id;
        $(".cover").show();
        $(".info-all").show();
        $(".full").attr('src',head_imgurl);
    }

    // 显示昵称审核弹出框
    function nickname(id,nick) {
        user_id = id;
        $(".cover").show();
        $(".nc-info").show();
        $("#nickname").text(nick);
    }
    //非法头像
    function del_img(){
        $.get("/user-info/del-avatar",{id:user_id,status:0},function() {
               location.reload();
        });
    }

    //非法昵称
    function del_nick() {
        $.get("/user-info/del-nickname",{id:user_id,status:0},function() {
               location.reload();
        });
    }

    
    // 关闭隐藏
    $("#sure").click(function(){
       $(".cover").hide();
       $(".info-all").hide();
    });

    
    /*$(".nicheng-show").click(function(){
        $(".cover").show();
        $(".nc-info").show();
    });*/

    // 关闭隐藏
    $("#nc-sure").click(function(){
       $(".cover").hide();
       $(".nc-info").hide();
    });
   
</script>