<?php
use yii\bootstrap\ActiveForm;
use app\models\Version;
?>
<script src="/DatePicker/WdatePicker.js" type="text/javascript"></script>
<script src="/js/downListSelect.js"></script>
<link href="/css/base.css" rel="stylesheet" type="text/css">
<script src="/js/jquery-1.9.1.min.js"></script>
<?php
$form = ActiveForm::begin([
    'id' => 'active-form',
    'action' => '/version/add',
    'options' => [
        'class' => 'form-signin',
        'enctype' => 'multipart/form-data'
    ]
]);

?>
<div class="cou-all">
        <div class="cou-title">
            <b>版本更新管理</b>&gt;&nbsp;<span class="add-mana">版本更新提醒</span>
            <a href="javascript:history.go(-1); " class="cou-return">返回</a>
        </div>
        <h4 class="tit">版本信息</h4>
        <div class="cou-sel">
            <div class="sel-left">
                <?= $form->field($model,'type')->dropDownList(Version::type(),[]);?>
            </div> 
            <div class="sel-right">
                <?= $form->field($model,'version')->input('text',[]);?>
            </div> 
            <div class="sel-right">
                <?= $form->field($model,'compel')->dropDownList(Version::compel(),[]);?>
            </div> 
        </div>

        <div class="cou-sel" style="margin-top:100px;">
            <div class="sel-left">
                <?= $form->field($model,'oper_time')->input('text',['onclick'=>"WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})",'style'=>'width:175px;']);?>   
            </div> 
            <div class="sel-right">
                <?= $form->field($model,'url')->input('text',[]);?>
            </div> 
        </div>
        <div class="add-info" style='margin-top:100px;'>
            <?= $form->field($model,'content')->textarea(['placeholder'=>'请填写更新内容']);?>
        </div> 

    <div class="chg-but" style="width:800px;">
        <input type='button' class="cou-edit"  value='取消'>
        <input type='submit' class="cou-edit" value='提交'>
    </div>

</div>
<?php ActiveForm::end(); ?>