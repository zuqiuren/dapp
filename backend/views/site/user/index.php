<?php
use yii\bootstrap\ActiveForm;
use backend\models\User;
?>
<ul class="breadcrumb">
			<li><a href="/">首页</a> <span class="divider">/</span></li>
			<li class="active">用户列表</li>
		</ul>
		<div class="container-fluid">
			<div class="row-fluid">
			

                    
<div class="btn-toolbar">
    <a href="/user/add"><button class="btn btn-primary"><i class="icon-plus"></i> 新建用户</button></a>
  <div class="btn-group">
  </div>
</div>
<div class="well">
    <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>用户名</th>
          <th>用户组</th>
          <th>邮箱</th>
          <th>状态</th>
          <th style="width: 26px;"></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($user as $val):?>
        <tr <?php if($val==0):?>style="background:#EEEEEE;"<?php endif;?>>
          <td><?php echo $val['id'];?></td>
          <td><?php echo $val['username'];?></td>
          <td><?php if(isset($val['g_name'])){ echo $val['g_name'];}else{ echo "";}?></td>
          <td><?php echo $val['email'];?></td>
          <td><?php echo User::getStatus($val['status']); ?></td>
          <td>
              <a href="/user/edit?id=<?php echo $val['id'];?>"><i class="icon-pencil"></i></a>
              <?php if($val['status']==1):?>
              <a href="#myModal" role="button" class="del" id="<?php echo $val['id'];?>" data-toggle="modal"><i class="icon-remove"></i></a>
              <?php endif;?>
          </td>
        </tr>
        <?php endforeach;?>
      </tbody>
    </table>
</div>
<div class="pagination">
    <ul>
        
    </ul>
</div>

<div class="modal small hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">删除确认</h3>
    </div>
    <div class="modal-body">
        <p class="error-text"><i class="icon-warning-sign modal-icon"></i>您真的要删除码？</p>
    </div>
    <div class="modal-footer">
        <button class="btn cancel" data-dismiss="modal" aria-hidden="true">取消</button>
        <button class="btn btn-danger" data-dismiss="modal">确定</button>
    </div>
</div>
<script type="text/javascript">
$(function(){
	   var id = 0;
        $('.btn-danger').click(function(){
            $.getJSON('/user/delete',{'id':id},function(json){
                  if(json.status==1){
              	       document.location.reload(true);
                  }
                  else{
                      alert(json.message);
                  }
             });
            return false;
        });

        $('.del').click(function(){
            id =$(this).attr('id');
        });
});
</script>                
