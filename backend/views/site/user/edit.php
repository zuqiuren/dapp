<?php
use yii\bootstrap\ActiveForm;
?>
<ul class="breadcrumb">
			<li><a href="/">首页</a> <span class="divider">/</span></li>
			<li class="active">添加用户</li>
		</ul>
		<div class="container-fluid">
			<div class="row-fluid">
                    
<div class="well">
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
               用户名：<?php echo $user->username;?><br /><br />
                密码&nbsp;&nbsp;&nbsp;&nbsp;：<input type="password" name="password" ><br /><br />
        <?= $form->field($user, 'email')->input('text',['class'=>'input-xlarge']) ?>
               所在分组：<?php foreach ($grouparr as $value):?>
            <input type="radio" name="gid" <?php if($user->gid==$value['id']) echo "checked";?> value="<?php echo $value['id']?>"><?php echo $value['name'];?>
            <?php endforeach;?><br /><br />
        <input type="submit" class="btn btn-primary" value="提交修改" />
       <?php ActiveForm::end(); ?>
      </div>
  </div>

</div>
<div class="modal small hide fade" id="message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">提示</h3>
    </div>
    <div class="modal-body">
        <p class="error-text"><i class="icon-warning-sign modal-icon"></i><span>删除成功</span></p>
    </div>
    <div class="modal-footer">
         <button class="btn cancel" data-dismiss="modal" aria-hidden="true">确定</button>
    </div>
</div>
<script>
$(function(){
	<?php if(Yii::$app->session->hasFlash('message')):?>
	$('#message').modal('show');
    $('.error-text span').html('<?php echo Yii::$app->session->getFlash('message');?>');
	<?php endif;?>
});
</script>