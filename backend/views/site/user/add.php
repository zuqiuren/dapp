<?php
use yii\bootstrap\ActiveForm;
?>
<style>
    input[type="radio"]:first-child {margin:-1px !important;}
                       
</style>
<ul class="breadcrumb">
			<li><a href="/">首页</a> <span class="divider">/</span></li>
			<li class="active">添加用户</li>
		</ul>
		<div class="container-fluid">
			<div class="row-fluid">
			

<div class="dialog">
        <div class="block">
            <p class="block-heading">添加用户</p>
            <div class="block-body">
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                     <?= $form->field($user, 'username')->input('text',['class'=>'span12']) ?>
                    <?= $form->field($user, 'email')->input('text',['class'=>'span12']) ?>
                    <?= $form->field($user, 'password')->passwordInput(['class'=>'span12']) ?>
                    <?php foreach($group as $value) {?>
                           <input type="radio" name='g_id' style="margin:0px !important;" checked value="<?php echo $value['id'];?>"><?php echo $value['name']?>
                    <?php }?>
                    <br />
                    <input type="submit" class="btn btn-primary pull-right" value="填加" />
                    <div class="clearfix"></div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
<div class="modal small hide fade" id="message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">提示</h3>
    </div>
    <div class="modal-body">
        <p class="error-text"><i class="icon-warning-sign modal-icon"></i><span>删除成功</span></p>
    </div>
    <div class="modal-footer">
         <button class="btn cancel" data-dismiss="modal" aria-hidden="true">确定</button>
    </div>
</div>

<script>
$(function(){
	<?php if(Yii::$app->session->hasFlash('message')):?>
    $('#message').modal('show');
    $('.error-text span').html('<?php echo Yii::$app->session->getFlash('message');?>');
	<?php endif;?>
});
</script>