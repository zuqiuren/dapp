<?php
use yii\bootstrap\ActiveForm;
use backend\models\Menu;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\web\UploadedFile;
?>
<script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/js/myForm.js" type="text/javascript"></script>
<script src="/kindeditor/kindeditor.js" type="text/javascript"></script>



<ul class="breadcrumb">
    <li>
        <a href="/">首页</a>
        <span class="divider">/</span>
    </li>
    <li class="active">添加</li>
</ul>

<?php
$form = ActiveForm::begin([
    'id' => 'active-form',
    'action' => '/article/add-market?type='.Yii::$app->request->get('type'),
    'options' => [
        'class' => 'form-signin',
        'enctype' => 'multipart/form-data'
    ],
]);

?>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="well">
            <div>
                <label class="control-label">图片</label>
                <?= FileInput::widget([
                    'name' => 'img_url',
                    'id'   => 'birockbrokecontent_img_url',
                    'options'=>[
                        'multiple'=>false
                    ],
                    'pluginOptions' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        'maxFilesNum' => 1,
                    ]
                ]);
                ?>
            </div>
            <div id='alt'><?= $form->field($model,'alt')->input('text',['style'=>'width: 320px;height: 30px;']); ?></div>
        </div>

        <div class="btn-toolbar">
            <button class="btn btn-primary" name="button" value="tj">

                <i class="icon-save"></i>保存</button>
            <a href="/article/market" data-toggle="modal" class="btn">取消</a>
            <div class="btn-group"></div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<script>
    $(function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        $('#message').modal('show');
        $('.error-text span').html('<?php echo Yii::$app->session->getFlash('message');?>');
        <?php endif;?>
    });

    $('#menu-channel').change(function()
    {
        $.ajax({
            url: "/menu/ajax-parentid",
            type: "post",
            //dataType: "json",
            data: {'channel': $(this).val()},
            success: function (data) {
                $('#menu-parent_id').html(data);
            }
        })
    });
</script>