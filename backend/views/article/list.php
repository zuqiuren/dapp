<?php
use yii\bootstrap\ActiveForm;
use backend\models\Admin;
use backend\models\Group;
?>
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
<link rel="stylesheet" href="/css/font-awesome.css">
<script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/js/myForm.js" type="text/javascript"></script>

<style>
    .modal{height:300px}
</style>
<ul class="breadcrumb">
    <li><a href="/">首页</a> <span class="divider">/</span></li>
    <li class="active">文章列表</li>
</ul>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="btn-toolbar">
            <a href="/article/add-content"><button  class="btn btn-primary btn-lg" style="margin-bottom:15px;"><i class="icon-plus"></i>添加文章</button></a>
            <a href="/article/add-product"><button  class="btn btn-primary btn-lg" style="margin-bottom:15px;"><i class="icon-plus"></i>添加产品</button></a>
        </div>

        <div class="well">
            <table class="table table-bordered table-hover definewidth m10">
                <thead>
                <tr>
                    <th>序号</th>
                    <th>文章标识</th>
                    <th>内容类型</th>
                    <th>分类名称</th>
                    <th>标题</th>
                    <th>描述</th>
                    <th>标签</th>
                    <th>创建时间</th>
                    <th>发布时间</th>
                    <th>创建人</th>
                    <th>推荐地</th>
                    <th>文章状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0;?>
                <?php foreach ($list as $val):?>
                    <tr <?php if($val==0):?>style="background:#EEEEEE;"<?php endif;?>>
                        <td><?php $i++; echo $i;?></td>
                        <td><?php echo $val['id'];?></td>
                        <td><?php if($val['type'] == 1):?>内容<?php else:?>产品<?php endif;?></td>
                        <td>
                            <?php $classify = \backend\models\ArticleClassify::findOne($val['pid']);?>
                            <?php echo $classify['name'];?>
                        </td>
                        <?php
                            if($val['type'] == 1)
                            {
                                $url = '/fr/websitepc/strategyDetail.html?id='.$val['id'];
                            }
                            else
                            {
                                $url = '/fr/websitepc/productDetail.html?id='.$val['id'];
                            }
                        ?>
                        <td><a target="_blank" href="<?= Yii::$app->params['host'] . $url?>"><?php if(strlen($val['title'])>60){ echo mb_substr($val['title'], 0, 20).'……';}else{echo $val['title'];}?></a></td>
                        <td><?php if(strlen($val['desc'])>60){ echo mb_substr($val['desc'], 0, 20).'……';}else{echo $val['desc'];}?></td>
                        <td><?php echo $val['keywords'];?></td>
                        <td><?php echo $val['create_time'];?></td>
                        <td><?php echo $val['publish_time'];?></td>
                        <td><?php echo $val['author'];?></td>
                        <td><?php if($val['position'] == 1):?>首页推荐<?php elseif($val['position'] == 2):?>侧栏推荐<?php elseif($val['position'] == 3):?>侧栏推荐2<?php endif;?></td>
                        <td><?php if($val['status'] == 1):?>草稿<?php elseif($val['status'] == 2):?>发布<?php else:?>撤回<?php endif;?></td>
                        <td>
                            <a href="<?php if($val['type'] == 1):?>/article/edit-content<?php else:?>/article/edit-product<?php endif;?>?id=<?= $val['id']?>" style="margin-right: 10px;cursor: pointer" ><i class="icon-pencil">编辑</i></a>
                                <a role="button" class="change" id="<?php echo $val['id'];?>" data-toggle="modal">
                                        <?php if($val['status']==1 || $val['status']==3):?>
                                            <i class="glyphicon glyphicon-hand-right"></i>发布
                                        <?php elseif($val['status']==2):?>
                                            <i class="glyphicon glyphicon-hand-left"></i>撤回
                                        <?php endif;?>
                                    </a>
                            <?php if($val['status']!=2):?><a role="button" onclick="del(<?= $val['id']?>)"><i class="glyphicon glyphicon-trash"></i> 删除</a><?php endif;?>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <div class="pagination">
            <ul>
                <?= $pageHtml?>
            </ul>
        </div>

    </div>
</div>
<script type="text/javascript">
    $('.btn-danger').click(function()
    {
        var name = $('#name').val();
        if(name)
            $('#active-form').submit();
    })
    function change(sub, name, status)
    {
        if(sub == 0)
        {
            $('#id').val(0);
        }
        else
        {
            $('#id').val(sub);
            $('#status').val(status);
            $('#name').val(name);
        }
    }
    $('.change').click(function()
    {
        var id = $(this).attr('id');
        $.post('/article/list-status',{'id':id},function(){
            document.location.reload(true);
        });
        return false;
    })

    function del(id)
    {
        var msg = confirm('确定么');
        if(msg)
        {
            $.post('/article/del',{'id':id},function(){
                document.location.reload(true);
            });
            return false;
        }
    }

</script>
