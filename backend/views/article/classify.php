<?php
use yii\bootstrap\ActiveForm;
use backend\models\Admin;
use backend\models\Group;
?>
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
<link rel="stylesheet" href="/css/font-awesome.css">
<script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/js/myForm.js" type="text/javascript"></script>

<style>
    .modal{height:300px}
</style>
<ul class="breadcrumb">
    <li><a href="/">首页</a> <span class="divider">/</span></li>
    <li class="active">分类列表</li>
</ul>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="btn-toolbar">
            <button  class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" style="margin-bottom:15px;"><i class="icon-plus"></i>新建分类</button>
        </div>

        <div class="well">
            <table class="table table-bordered table-hover definewidth m10">
                <thead>
                <tr>
                    <th>序号</th>
                    <th>分类标识</th>
                    <th>上级分类</th>
                    <th>分类名称</th>
                    <th>创建时间</th>
                    <th>创建人</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 0;?>
                <?php foreach ($list as $val):?>
                    <tr <?php if($val==0):?>style="background:#EEEEEE;"<?php endif;?>>
                        <td><?php $i++; echo $i;?></td>
                        <td><?php echo $val['id'];?></td>
                        <td><?php echo '--';?></td>
                        <td><?php echo $val['name'];?></td>
                        <td><?php echo $val['create_time'];?></td>
                        <td><?php echo $val['author'];?></td>
                        <td><?php if ($val['status'] == 0):?>隐藏<?php else:?>显示<?php endif;?></td>
                        <td>
                            <a onclick="change(<?= $val['id']?>,'<?=$val["name"]?>','<?=$val["status"]?>')" style="margin-right: 10px;cursor: pointer" data-toggle="modal" data-target="#myModal"><i class="icon-pencil">编辑</i></a>
                            <?php if($val['status']==1):?>
                                <a role="button" class="change" id="<?php echo $val['id'];?>" data-toggle="modal"><i class="icon-remove">隐藏</i></a>
                            <?php elseif($val['status']==0):?>
                                <a role="button" class="change" id="<?php echo $val['id'];?>" data-toggle="modal"><i class="icon-remove">显示</i></a>
                            <?php endif;?>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <div class="pagination">
            <ul>

            </ul>
        </div>

        <?php
        $form = ActiveForm::begin([
            'id' => 'active-form',
            'action' => '/article/add-classify',
            'options' => [
                'class' => 'form-signin',
                'enctype' => 'multipart/form-data'
            ],
        ]);
        ?>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">添加/修改分类</h4>
            </div>
            <input type="hidden" name="id" value="0" id="id">
            <div class="modal-body">
                <p class="error-text">上级分类 ：<select style="width: 250px;height: 30px;"><option value="0">----</option></select></p>
                <p class="error-text">分类名称 ：<input id="name" id="name" style="width: 250px;height: 30px;" type="text" name="name" placeholder="如：旅游"></p>
                <p class="error-text">分类状态 ：<select id="status" name="status" style="width: 250px;height: 30px;"><option value="1">显示</option><option value="0">隐藏</option></select></p>
            </div>
            <div class="modal-footer">
                <button class="btn cancel" data-dismiss="modal" aria-hidden="true">取消</button>
                <button class="btn btn-danger" data-dismiss="modal">确定</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        </div>
    </div>
        <script type="text/javascript">
           $('.btn-danger').click(function()
           {
               var name = $('#name').val();
               if(name)
                   $('#active-form').submit();
           })
            function change(sub, name, status)
            {
                if(sub == 0)
                {
                    $('#id').val(0);
                }
                else
                {
                    $('#id').val(sub);
                    $('#status').val(status);
                    $('#name').val(name);
                }
            }
            $('.change').click(function()
            {
                var id = $(this).attr('id');
                $.post('/article/classify-status',{'id':id},function(){
                        document.location.reload(true);
                });
                return false;
            })

        </script>
