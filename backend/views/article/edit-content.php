<?php
use yii\bootstrap\ActiveForm;
use backend\models\Menu;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\web\UploadedFile;
?>
<script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/js/myForm.js" type="text/javascript"></script>
<script src="/kindeditor/kindeditor.js" type="text/javascript"></script>

<style>
    #menu-is_show .radio{float:left;}
    #menu-is_show .radio:nth-of-type(2){margin-top: 10px;}
    #article-position .radio{display:inline-block;}
</style>

<script>
    var editor;
    KindEditor.ready(function(K) {
        editor = K.create('#article-content', {
            afterBlur:function()
            {
                this.sync();
            }
        })});
</script>

<ul class="breadcrumb">
    <li>
        <a href="/">首页</a>
        <span class="divider">/</span>
    </li>
    <li class="active">添加菜单</li>
</ul>

<?php
$form = ActiveForm::begin([
    'id' => 'active-form',
    'action' => '/article/edit-content?id='.Yii::$app->request->get('id'),
    'options' => [
        'class' => 'form-signin',
        'enctype' => 'multipart/form-data'
    ],
]);

?>
<input type="hidden" name="Article[id]" value="<?= $model->id;?>">
<div class="container-fluid">
    <div class="row-fluid">

        <div class="well">
            <label class="control-label">所属分类</label>
            <div>
                <?php
                echo Html::activeDropDownList($model, 'pid', ArrayHelper::map(\backend\models\ArticleClassify::find()->where(['status' => 1])->all(),'id', 'name'), ['style' => 'width: 320px;height: 40px;border-radius: 5px;box-shadow: 0 0 5px #ccc;position: relative;']);
                ?>
            </div>
            <div id='name'><?= $form->field($model,'title')->input('text',['style'=>'width: 320px;height: 30px;']); ?></div>
            <label class="control-label">图片尺寸215*118</label>
            <div>
                <?php
                $img = '';
                if($model->head_img){
                    $img = Yii::$app->params['img_url']. '/'.$model->head_img;
                }
                ?>
                <?= FileInput::widget([
                    'name' => 'head_img',
                    'id'   => 'birockbrokecontent_img_url',
                    'options'=>[
                     //   'multiple'=>true
                    ],
                    'pluginOptions' => [
                        'showUpload' => false,
                        'showRemove' => false,
                        'initialPreview'=> [
                            "<img src='" . $img . "' style='width: 100%; height: 160px;' class='file-preview-image' alt='picture' title='picture'>",
                        ],
                    ]
                ]);
                ?>

            </div>

            <div id='alt'><?= $form->field($model,'head_img_alt')->input('text',['style'=>'width: 320px;height: 30px;']); ?></div>
            <div id='content'><?= $form->field($model,'content')->textarea(['rows'=>15]); ?></div>
            <div id='content'><?= $form->field($model,'keywords')->input('text',['style'=>'width: 320px;height: 30px;']); ?></div>
            <div id='content'><?= $form->field($model,'desc')->textarea(['rows'=>4]); ?></div>
            <div id='position'><?= $form->field($model,'position')->radioList(['0'=> '无',1=>'首页推荐',2=>'侧栏推荐',3=>'侧栏推荐2'],[1]); ?></div>
        </div>

        <div class="btn-toolbar">
            <button class="btn btn-primary" name="button" value="tj">

                <i class="icon-save"></i>保存</button>
            <a href="/article/list" data-toggle="modal" class="btn">取消</a>
            <div class="btn-group"></div>
        </div>

    </div>
</div>

<?php ActiveForm::end(); ?>
<div class="modal small hide fade" id="message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">提示</h3>
    </div>
    <div class="modal-body">
        <p class="error-text"><i class="icon-warning-sign modal-icon"></i><span>删除成功</span></p>
    </div>
    <div class="modal-footer">
        <button class="btn cancel" data-dismiss="modal" aria-hidden="true">确定</button>
    </div>
</div>

<script>
    $(function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        $('#message').modal('show');
        $('.error-text span').html('<?php echo Yii::$app->session->getFlash('message');?>');
        <?php endif;?>
    });
    $("#parentid").change(function() {
        var parent_id = $("#parentid").val();
        if(parent_id == 0){
            $("#mark").show();
            $("#weight").show();
            $("#url").hide();
        }
        else
        {
            $("#mark").hide();
            $("#weight").hide();
            $("#url").show();
        }
    });

    $(function() {
        var parent_id = $("#parentid").val();
        if(parent_id == 0)
        {
            $("#mark").show();
            $("#weight").show();
            $("#url").hide();
        }else
        {
            $("#mark").hide();
            $("#weight").hide();
            $("#url").show();
        }
    });
    $('#menu-channel').change(function()
    {
        $.ajax({
            url: "/menu/ajax-parentid",
            type: "post",
            //dataType: "json",
            data: {'channel': $(this).val()},
            success: function (data) {
                $('#menu-parent_id').html(data);
            }
        })
    })
</script>