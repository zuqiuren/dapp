<?php
use yii\bootstrap\ActiveForm;
use backend\models\Admin;
use backend\models\Group;
?>
<link rel="stylesheet" href="/css/font-awesome.css">
<script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/js/myForm.js" type="text/javascript"></script>

<style>
    .modal{height:300px}
</style>
<ul class="breadcrumb">
    <li><a href="/">首页</a> <span class="divider">/</span></li>
    <li class="active">利率</li>
</ul>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="btn-toolbar">
            <button  class="btn btn-primary btn-lg"  data-toggle="modal" data-target="#myModal" style="margin-bottom:15px;"><i class="icon-plus"></i>添加</button>
        </div>
        <div class="well">
            <table class="table table-bordered table-hover definewidth m10">
                <thead>
                <tr><td colspan="3" style="text-align: center;text-shadow:5px 2px 6px #000;font-weight:bold">全国</td></tr>
                <tr>
                    <th>内容名称</th>
                    <th>利率内容</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data as $k=>$val):?>
                    <tr <?php if($k==0):?>style="background:#EEEEEE;"<?php endif;?>>
                        <td><input type="text" id="quanname<?= $val['id']?>" name="name" value="<?= $val['name']?>"></td>
                        <td><input type="text" id="quanrate<?= $val['id']?>" name="rate" value="<?= $val['rate']?>"></td>
                        <td>
                            <i style="cursor: pointer" onclick="alter(<?= $val['id']?>)" class="icon-pencil">修改</i>&nbsp;
                            <i role="button" onclick="del(<?= $val['id']?>)" class="glyphicon glyphicon-trash">删除</i>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>

            <table class="table table-bordered table-hover definewidth m10">
                <thead>
                <tr><td colspan="3" style="text-align: center;text-shadow:5px 2px 6px #000;font-weight:bold">天津</td></tr>
                <tr>
                    <th>内容名称</th>
                    <th>利率内容</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($tianjin as $k=>$val):?>
                    <tr <?php if($k==0):?>style="background:#EEEEEE;"<?php endif;?>>
                        <td><input type="text" id="quanname<?= $val['id']?>" name="name" value="<?= $val['name']?>"></td>
                        <td><input type="text" id="quanrate<?= $val['id']?>" name="rate" value="<?= $val['rate']?>"></td>
                        <td>
                            <i role="button" onclick="alter(<?= $val['id']?>)" class="icon-pencil">修改</i>&nbsp;
                            <i role="button" onclick="del(<?= $val['id']?>)" class="glyphicon glyphicon-trash">删除</i>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <?php
        $form = ActiveForm::begin([
            'id' => 'active-form',
            'action' => '/article/add-rate',
            'options' => [
                'class' => 'form-signin',
                'enctype' => 'multipart/form-data'
            ],
        ]);
        ?>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">添加利率</h4>
            </div>
            <input type="hidden" name="id" value="0" id="id">
            <div class="modal-body">
                <p class="error-text">内容名称 ：<input id="name" name="name" style="width: 250px;height: 30px;" type="text" placeholder="请填写内容名"></p>
                <p class="error-text">利率内容 ：<input id="rate" name="rate" style="width: 250px;height: 30px;" type="text" name="name" placeholder="请填写利率内容"></p>
                <p class="error-text">所属区域 ：<select name="pid" style="width: 250px;height: 30px;"><option value="0">全国</option><option value="1">天津</option></select></p>
            </div>
            <div class="modal-footer">
                <button class="btn cancel" data-dismiss="modal" aria-hidden="true">取消</button>
                <button class="btn btn-danger" data-dismiss="modal">确定</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<script type="text/javascript">
    $('.btn-danger').click(function()
    {
        var name = $('#name').val();
        var rate = $('#rate').val();
        if(name && rate)
        {
            $('#active-form').submit();
        }
        else
        {
            alert('信息不全！');
            return false;
        }

    })
    function alter(id)
    {
        var name = $('#quanname'+id).val();
        var rate = $('#quanrate'+id).val();
        $.post('/article/edit-rate',{'id':id, 'name':name, 'rate':rate},function(){
            document.location.reload(true);
        });
    }
    function del(id)
    {
        var msg = confirm('确定么');
        if(msg)
        {
            $.post('/article/del-rate',{'id':id},function(){
                document.location.reload(true);
            });
            return false;
        }
    }
</script>
