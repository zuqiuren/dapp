<?php
use yii\bootstrap\ActiveForm;
use backend\models\Menu;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\web\UploadedFile;
?>
<script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="/css/font-awesome.css">
<script src="/js/myForm.js" type="text/javascript"></script>
<script src="/kindeditor/kindeditor.js" type="text/javascript"></script>
<style>
.lunbo .unit{width:300px;display:inline-block;}
</style>
<ul class="breadcrumb">
    <li>
        <a href="/">首页</a>
        <span class="divider">/</span>
    </li>
    <li class="active">营销</li>
</ul>


<div class="container-fluid">
    <div class="row-fluid">
        <div class="well">
            <label class="control-label" style="text-shadow:5px 2px 8px #000;font-weight:bold">轮播图 &nbsp;&nbsp;&nbsp;&nbsp; <?php if(count($lunbo) < 4):?><a href="/article/add-market?type=1"><div style="float:right" class="glyphicon glyphicon-plus">增加</div></a><?php endif;?></label>
                <div class="lunbo">
                <?php foreach($lunbo as $v):?>
                <div class="unit">
                    <img style="width:300px;height:200px" src="<?= Yii::$app->params['img_url']. '/'.$v->img_url?>">
                    <div id='name'>
                        alt:<input type="text" value="<?= $v->alt?>" name="alt" style='width: 200px;height: 30px;'>
                        <a href="/article/edit-market?id=<?= $v->id?>"><i class="icon-pencil">改</i></a>  &nbsp;&nbsp;  <a onclick="return confirm('确定删除?')" href="/article/del-market?id=<?= $v->id?>"><i class="glyphicon glyphicon-trash"></i>删</a>
                    </div>
                </div>
                <?php endforeach;?>
                </div>
            <label class="control-label" style="text-shadow:5px 2px 8px #000;font-weight:bold">平台特色 &nbsp;&nbsp;&nbsp;&nbsp; <?php if(count($feature) < 4):?><a href="/article/add-market?type=2"><div style="float:right" class="glyphicon glyphicon-plus">增加</div></a><?php endif;?></label>
            <div class="lunbo">
                <?php foreach($feature as $v):?>
                    <div class="unit">
                        <img style="width:300px;height:200px" src="<?= Yii::$app->params['img_url']. '/'.$v->img_url?>">
                        <div id='name'>
                            alt:<input type="text" value="<?= $v->alt?>" name="alt" style='width: 200px;height: 30px;'>
                            <a href="/article/edit-market?id=<?= $v->id?>"><i class="icon-pencil">改</i></a>  &nbsp;&nbsp;
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
            <label class="control-label" style="text-shadow:5px 2px 8px #000;font-weight:bold">平台服务 &nbsp;&nbsp;&nbsp;&nbsp; <?php if(count($service) < 1):?><a href="/article/add-market?type=3"><div style="float:right" class="glyphicon glyphicon-plus">增加</div></a><?php endif;?></label>
            <div class="lunbo">
                <?php foreach($service as $v):?>
                    <div class="unit">
                        <img style="width:300px;height:200px" src="<?= Yii::$app->params['img_url']. '/'.$v->img_url?>">
                        <div id='name'>
                            alt:<input type="text" value="<?= $v->alt?>" name="alt" style='width: 200px;height: 30px;'>
                            <a href="/article/edit-market?id=<?= $v->id?>"><i class="icon-pencil">改</i></a>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
            <label class="control-label" style="text-shadow:5px 2px 8px #000;font-weight:bold">产品大全 &nbsp;&nbsp;&nbsp;&nbsp; <?php if(count($product) < 1):?><a href="/article/add-market?type=4"><div style="float:right" class="glyphicon glyphicon-plus">增加</div></a><?php endif;?></label>
            <div class="lunbo">
                <?php foreach($product as $v):?>
                    <div class="unit">
                        <img style="width:300px;height:200px" src="<?= Yii::$app->params['img_url']. '/'.$v->img_url?>">
                        <div id='name'>
                            alt:<input type="text" value="<?= $v->alt?>" name="alt" style='width: 200px;height: 30px;'>
                            <a href="/article/edit-market?id=<?= $v->id?>"><i class="icon-pencil">改</i></a>  &nbsp;&nbsp;
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
            <label class="control-label" style="text-shadow:5px 2px 8px #000;font-weight:bold">攻略贷款 &nbsp;&nbsp;&nbsp;&nbsp; <?php if(count($stra) < 1):?><a href="/article/add-market?type=5"><div style="float:right" class="glyphicon glyphicon-plus">增加</div></a><?php endif;?></label>
            <div class="lunbo">
                <?php foreach($stra as $v):?>
                    <div class="unit">
                        <img style="width:300px;height:200px" src="<?= Yii::$app->params['img_url']. '/'.$v->img_url?>">
                        <div id='name'>
                            alt:<input type="text" value="<?= $v->alt?>" name="alt" style='width: 200px;height: 30px;'>
                            <a href="/article/edit-market?id=<?= $v->id?>"><i class="icon-pencil">改</i></a>  &nbsp;&nbsp;
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
            <label class="control-label" style="text-shadow:5px 2px 8px #000;font-weight:bold">保证服务 &nbsp;&nbsp;&nbsp;&nbsp; <?php if(count($prom) < 1):?><a href="/article/add-market?type=6"><div style="float:right" class="glyphicon glyphicon-plus">增加</div></a><?php endif;?></label>
            <div class="lunbo">
                <?php foreach($prom as $v):?>
                    <div class="unit">
                        <img style="width:300px;height:200px" src="<?= Yii::$app->params['img_url']. '/'.$v->img_url?>">
                        <div id='name'>
                            alt:<input type="text" value="<?= $v->alt?>" name="alt" style='width: 200px;height: 30px;'>
                            <a href="/article/edit-market?id=<?= $v->id?>"><i class="icon-pencil">改</i></a>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
            <label class="control-label" style="text-shadow:5px 2px 8px #000;font-weight:bold">关于我们 &nbsp;&nbsp;&nbsp;&nbsp; <?php if(count($about) < 1):?><a href="/article/add-market?type=7"><div style="float:right" class="glyphicon glyphicon-plus">增加</div></a><?php endif;?></label>
            <div class="lunbo">
                <?php foreach($about as $v):?>
                    <div class="unit">
                        <img style="width:300px;height:200px" src="<?= Yii::$app->params['img_url']. '/'.$v->img_url?>">
                        <div id='name'>
                            alt:<input type="text" value="<?= $v->alt?>" name="alt" style='width: 200px;height: 30px;'>
                            <a href="/article/edit-market?id=<?= $v->id?>"><i class="icon-pencil">改</i></a>  &nbsp;&nbsp;
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
            <label class="control-label" style="text-shadow:5px 2px 8px #000;font-weight:bold">社区论坛 &nbsp;&nbsp;&nbsp;&nbsp; <?php if(count($forum) < 1):?><a href="/article/add-market?type=8"><div style="float:right" class="glyphicon glyphicon-plus">增加</div></a><?php endif;?></label>
            <div class="lunbo">
                <?php foreach($forum as $v):?>
                    <div class="unit">
                        <img style="width:300px;height:200px" src="<?= Yii::$app->params['img_url']. '/'.$v->img_url?>">
                        <div id='name'>
                            alt:<input type="text" value="<?= $v->alt?>" name="alt" style='width: 200px;height: 30px;'>
                            <a href="/article/edit-market?id=<?= $v->id?>"><i class="icon-pencil">改</i></a>  &nbsp;&nbsp;
                        </div>
                    </div>
                <?php endforeach;?></div>
        </div>
    </div>
    <div class="btn-toolbar">

    </div>

</div>

<div class="modal small hide fade" id="message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">提示</h3>
    </div>
    <div class="modal-body">
        <p class="error-text"><i class="icon-warning-sign modal-icon"></i><span>删除成功</span></p>
    </div>
    <div class="modal-footer">
        <button class="btn cancel" data-dismiss="modal" aria-hidden="true">确定</button>
    </div>
</div>

<script>
    $(function(){
        <?php if(Yii::$app->session->hasFlash('message')):?>
        $('#message').modal('show');
        $('.error-text span').html('<?php echo Yii::$app->session->getFlash('message');?>');
        <?php endif;?>
    });
    $("#parentid").change(function() {
        var parent_id = $("#parentid").val();
        if(parent_id == 0){
            $("#mark").show();
            $("#weight").show();
            $("#url").hide();
        }
        else
        {
            $("#mark").hide();
            $("#weight").hide();
            $("#url").show();
        }
    });

    $(function() {
        var parent_id = $("#parentid").val();
        if(parent_id == 0)
        {
            $("#mark").show();
            $("#weight").show();
            $("#url").hide();
        }else
        {
            $("#mark").hide();
            $("#weight").hide();
            $("#url").show();
        }
    });
    $('#menu-channel').change(function()
    {
        $.ajax({
            url: "/menu/ajax-parentid",
            type: "post",
            //dataType: "json",
            data: {'channel': $(this).val()},
            success: function (data) {
                $('#menu-parent_id').html(data);
            }
        })
    });
</script>