<?php
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<ul class="breadcrumb">
			<li><a href="/">首页</a> <span class="divider">/</span></li>
			<li class="active">编辑角色</li>
		</ul>
		<div class="container-fluid">
			<div class="row-fluid">

<?php
    $form = ActiveForm::begin([
        'id' => 'active-form',
        'options' => [
            'class' => 'form-signin',
            'enctype' => 'multipart/form-data'
        ],
    ]);

?>

<?= $form->field($model,'name')->input('text',['class'=>'input-xlarge','value'=>$model['name']]); ?>

<?php foreach ($menu as $key=>$value):?>


<?php if(isset($value['name'])):?>
<h4 style="margin: 25px;"><?php echo $value['name'];  ?></h4>
<?php endif;?>
<?php if(isset($value['find'])):?>
<?php foreach ($value['find'] as $k=>$val):?>
<!-- <span style="padding:0px 50px;background:#fff;width:150px;display:inline-block;height:35px;line-height:35px;">
<input type="checkbox" name="mid[]" <?php if(isset($model['mid'][$k])){ if($model['mid'][$k]==$val['id']):?> checked <?php endif;}?> value='<?php echo $val['id'];?>'><?php echo $val['name'] ?>
</span>-->


<span style="padding:0px 30px;background:#fff;width:150px;display:inline-block;height:35px;line-height:35px;">
<input type="checkbox" name="mid[]" <?php foreach($model['mid'] as $v){ if($v==$val['id']):?> checked <?php endif;}?> value='<?php echo $val['id'];?>'><?php echo $val['name'] ?>
</span>



<?php endforeach;?>
<?php endif;?>
<?php endforeach;?>
<br />
<input type="submit" style="margin-top:50px;width:80px;" class="btn btn-primary" value="添加" />
<?php ActiveForm::end(); ?>

<script>
    $(function () {       
		$('#backid').click(function(){
				window.location.href="/game/";
		 });

    });
</script>