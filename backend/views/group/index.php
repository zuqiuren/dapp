<?php
use backend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);

use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;

use yii\helpers\ArrayHelper;
use backend\models\Gameinfo;

?>
<ul class="breadcrumb">
			<li><a href="/">首页</a> <span class="divider">/</span></li>
			<li class="active">角色列表</li>
		</ul>
		<div class="container-fluid">
			<div class="row-fluid">


<table class="table table-bordered table-hover definewidth m10">

    <thead>
    	<tr>
    		<th style="width:100px;">分组编号</th>
    		<th style="width:150px;">分组名称</th>
			<th>所属系统</th>
    		<th>分组用户</th>
    		<th style="width:150px;">操作</th>
    	</tr>
    </thead>
    
    <?php foreach ($group as $key => $value) {?>
    
        <tr>
			<td><?=$value['id'];?></td>
			<td><?=$value['name'];?></td>
			<td><?php if(strpos($value['channel'], 'admin') !== false):?>后台管理系统<?php endif;?><?php if(strpos($value['channel'], 'yunying') !== false):?> 运营管理系统<?php endif;?><?php if(strpos($value['channel'], 'config') !== false):?> 配置后台<?php endif;?></td>
			<td>
			<?php if(isset($value['username'])){ foreach ($value['username'] as $val) {?>
			     <span style="width:100px;display:inline-block;"><?=$val?></span>
			<?php }}?>
			</td>
			<td>
			 <a href="/group/edit?id=<?=$value['id']?>"><i class="icon-pencil" title="编辑">编辑</i></a>
			</td>
		</tr>
    <?php }?>
    
</table>



