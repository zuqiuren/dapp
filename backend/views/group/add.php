<?php
use yii\bootstrap\ActiveForm;
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<ul class="breadcrumb">
			<li><a href="/">首页</a> <span class="divider">/</span></li>
			<li class="active">添加角色</li>
		</ul>
		<div class="container-fluid">
			<div class="row-fluid">

<?php
    $form = ActiveForm::begin([
        'id' => 'active-form',
        'action' => '/group/add',
        'options' => [
            'class' => 'form-signin',
            'enctype' => 'multipart/form-data'
        ],
    ]);

?>

<?= $form->field($model,'name')->input('text',['class'=>'input-xlarge']); ?>

				<?= $form->field($model, 'channel')->checkboxList(['admin'=>'后台管理系统','yunying'=>'运营管理系统','config'=>'配置后台','article'=>'官网后台'],['value'=>['admin'],'item'=>function($index, $label, $name, $checked, $value){
					$checkStr = $checked?"checked":"";
					return '<label><input type="checkbox" name="'.$name.'" value="'.$value.'" '.$checkStr.' class="class'.$index.'" data-uid="user'.$index.'">'.$label.'</label>';
				},'itemOptions'=>['class'=>'myClass']]); ?>

				<br>
<table>
<?php foreach ($module as $key=>$value):?>
<h4 style="margin: 25px;"><ul><?php echo $value['name'] ?></ul></h4>
<?php if(isset($value['find'])):?>
<?php foreach ($value['find'] as $val):?>
<span style="padding:0px 30px;background:#fff;width:200px;display:inline-block;height:35px;line-height:35px;">
<input style="margin:auto !important" type="checkbox" name="mid[]" value='<?php echo $val['id'];?>'> <?php echo $val['name'] ?>
</span>
<?php endforeach;?>
<?php endif;?>
<?php endforeach;?>
<br />
<input type="submit" style="margin-top:50px;width:80px;" class="btn btn-primary" value="添加" />
</table>
<?php ActiveForm::end(); ?>

<script>
    $(function () {       
		$('#backid').click(function(){
				window.location.href="/game/";
		 });

    });
</script>