<?php
use yii\bootstrap\ActiveForm;
?>
<ul class="breadcrumb">
    <li><a href="/">首页</a> <span class="divider">/</span></li>
    <li class="active">日志列表</li>
</ul>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="row">
            <fieldset>
                <div class="form-group col-md-12">
                    <form  class="form-search" action="/log/list" method="get">
                        <select  class="span2 search-query" style="width: 130px;height: 30px; float:left; margin-right: 10px;" name="url">
                            <option <?php if($search['url'] == '') echo 'selected';?> value="">选择页面</option>
                            <?php foreach($menu as $key => $val):?>
                                <option <?php if($search['url'] == $val['url']) echo 'selected';?> value="<?= $val['url'];?>"><?= $val['name']?></option>
                            <?php endforeach;?>
                        </select>
                        <div class="input-append">
                        <input type="text" name="username" class="span2 search-query" style="width:130px;height: 30px; float:left;" value="<?= $search['username'] ?>" placeholder="用户名"  />

                        <button type="submit" style=" float:left;"  class="btn">搜索</button>
                            </div>
                    </form>
                </div>
            </fieldset>
        </div>
        <div class="well">
            <table class="table table-bordered table-hover definewidth m10">

                <thead>
                <tr>
                    <th>操作者</th>
                    <th>操作内容</th>
                    <th>时间</th>
                </tr>
                </thead>
                <?php foreach ($data['list'] as $key => $value) :?>
                    <tr>
                        <td><?=$value['name'];?></td>
                        <td><?= json_decode($value['content']);?></td>
                        <td>
                            <?= date('Y-m-d H:i:s', $value['datetime'])?>
                        </td>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>

        <div class="col-sm-7 ">
            <div id="table_list_paginate" class="dataTables_paginate paging_simple_numbers">
                <ul class="pagination">
                    <?php echo $pageHtml;?>
                </ul>
            </div>
        </div>
        </div>
    </div>