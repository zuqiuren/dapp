<?php
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<ul class="breadcrumb">
    <li><a href="/">轮播图</a> <span class="divider"></span></li>
    <li class="active">列表</li>
</ul>
<a href="/index/addlunbo"><button style="background-color: #367fa9;border-color: #00a7d0;color:whitesmoke">添加轮播图</button></a><br><br>
<div class="container-fluid">
    <div class="row-fluid">
        <?= GridView::widget([
            'dataProvider' => $data,
            'emptyText' => '暂无数据！',
            //'layout' => "{items}\n{pager}",
            'columns' => [
                [
                    'label' => 'id',
                    'attribute' => 'id',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->id;
                    },
                ],
                [
                    'label' => '图片',
                    'attribute' => 'img',
                    'format' => 'raw',
                    'value' => function($data) {
                        return "<img style='width:80px;height:80px;' src='".Yii::$app->params['img_url'].'/'.$data->img."'>";
                    },
                ],
                [
                    'label' => '所属页面',
                    'attribute' => 'style',
                    'format' => 'raw',
                    'value' => function($data) {
                        switch($data->style)
                        {
                            case '1':
                                return '首页';
                            break;
                            case '2':
                                return '开放实践活动';
                            break;
                            case '3':
                                return '创客课程';
                                break;
                            case '4':
                                return '夏令营';
                                break;
                            case '5':
                                return '留学背景';
                                break;
                            case '6':
                                return '名师团队';
                                break;
                        }
                    },
                ],
                [
                    'label' => '链接',
                    'attribute' => 'url',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->url;
                    },
                ],
                [
                    'label' => '创建时间',
                    'attribute' => 'datetime',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->datetime;
                    },
                ],
                [
                    'label' => '操作',
                    'attribute' => '',
                    'format' => 'raw',
                    'value' => function($data) {
                        return  html::a('<span  onclick="return confirm(\'确定删除这条么\')" class="label label-info" title="删除"><i class="fa fa-info-circle fa-lg"></i></span>', Url::to(['index/dellunbo', 'id' => $data->id])) . '&nbsp;&nbsp;' .
                        html::a('<span class="label label-primary" title="编辑"><i class="fa fa-cog fa-lg"></i></span>', Url::to(['index/uplunbo', 'id' => $data->id]));
                    },
                ],
            ]])?>


    </div>

</div>
