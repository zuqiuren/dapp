<?php
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<ul class="breadcrumb">
    <li><a href="/">夏令营</a> <span class="divider"></span></li>
    <li class="active">列表</li>
</ul>
<a href="/index/addopenpractice"><button style="background-color: #367fa9;border-color: #00a7d0;color:whitesmoke">添加实践活动</button></a><br><br>
<div class="container-fluid">
    <div class="row-fluid">
        <?= GridView::widget([
            'dataProvider' => $data,
            'emptyText' => '暂无数据！',
            //'layout' => "{items}\n{pager}",
            'columns' => [
                [
                    'label' => 'id',
                    'attribute' => 'id',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->id;
                    },
                ],
                [
                    'label' => 'img',
                    'attribute' => '小图',
                    'format' => 'raw',
                    'value' => function($data) {
                        return "<img style='width:80px;height:80px;' src='".Yii::$app->params['img_url'].'/'.$data->img."'>";
                    },
                ],
                [
                    'label' => 'img',
                    'attribute' => '大图',
                    'format' => 'raw',
                    'value' => function($data) {
                        return "<img style='width:80px;height:80px;' src='".Yii::$app->params['img_url'].'/'.$data->img_big."'>";
                    },
                ],
                [
                    'label' => '标题',
                    'attribute' => 'title',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->title;
                    },
                ],
                [
                    'label' => '操作',
                    'attribute' => '',
                    'format' => 'raw',
                    'value' => function($data) {
                        return  html::a('<span  onclick="return confirm(\'确定删除这条么\')" class="label label-info" title="删除"><i class="fa fa-info-circle fa-lg"></i></span>', Url::to(['index/delopenpractice', 'id' => $data->id])) . '&nbsp;&nbsp;' .
                        html::a('<span class="label label-primary" title="编辑"><i class="fa fa-cog fa-lg"></i></span>', Url::to(['index/upopenpractice', 'id' => $data->id]));
                    },
                ],
            ]])?>


    </div>

</div>
