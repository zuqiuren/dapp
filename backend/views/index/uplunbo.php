<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\file\FileInput;

?>
<style type="text/css">
    .select{width:150px;}
</style>


<?php
$form = ActiveForm::begin([
    'options' => [
        'class' => 'form-signin',
        'id' => 'form2',
        'name' => 'form1',
        'enctype' => 'multipart/form-data'
        // 'onSubmit' => 'return msg_form(this)'
    ]
]);
?>

<div class="container-fluid">
    <div class="row-fluid">

        <div class="well">
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane active in" id="home">

                    <table border="0" cellpadding="0" cellspacing="0" id="table">
                        <tr>

                            <td height="25" width="90%"><?= $form->field($model, 'style')->dropDownList([1 => '首页' , 2 => '开放实践活动' , 3 => '创客课程' , 4 => '夏令营' , 5 => '留学背景' ,6 => '名师团队', 7 => '合作定制' , 8 => '关于我们']);?></td>

                        </tr>
                        <tr>
                            <td height="25" width="90%">
                                <?= FileInput::widget([
                                    'name' => 'img',
                                    'id'   => 'birockbrokecontent_img_url',
                                    'options'=>[
                                        'multiple'=>true
                                    ],
                                    'pluginOptions' => [
                                        'showUpload' => false,
                                        'showRemove' => false,
                                        'initialPreview'=> [
                                            "<img src='" .  Yii::$app->params['img_url']. '/'.$model->img . "' style='width: 100%; height: 160px;' class='file-preview-image' alt='picture' title='picture'>",
                                        ],
                                    ]
                                ]);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td height="25" width="90%"><?php echo $form->field($model, 'url')->input('text');?></td>
                        </tr>
                        <tr>
                            <td height="25" width="90%">
                                <?= Html::submitButton('修改', ['class' => 'btn btn-primary btn-primary-2 btn-lg', 'name' => 'contact-button']) ?>

                            </td>
                        </tr>
                    </table>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>


