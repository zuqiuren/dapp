<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<script src="/kindeditor/kindeditor-min.js"></script>
<script src="/kindeditor/lang/zh_CN.js"></script>
<script src="/js/jquery-1.7.1.min.js"></script>
<link href="/kindeditor/themes/default/default.css">

<script>
    var editor;
    KindEditor.ready(function(K)
    {
        editor = K.create("#openpracticeinfo-content",{
            height : "350px",
            allowFileManager:true
        })
    })
</script>
<style type="text/css">
    .select{width:150px;}
</style>


<?php
$form = ActiveForm::begin([
    'options' => [
        'class' => 'form-signin',
        'id' => 'form2',
        'name' => 'form1',
        // 'onSubmit' => 'return msg_form(this)'
    ]
]);
?>

<div class="container-fluid">
    <div class="row-fluid">

        <div class="well">
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane active in" id="home">
                    <table border="0" cellpadding="0" cellspacing="0" id="table">
                        <tr>
                            <td width="30" height="60">&nbsp;</td>
                            <td height="100" width="90%"><?= $form->field($model, 'content')->textarea()->label(false); ?></td>
                        </tr>
                        <tr>
                            <td width="30" height="60">&nbsp;</td>
                            <td height="25" width="90%"  align="center">
                                <?= Html::submitButton('修改', ['class' => 'btn btn-primary btn-primary-2 btn-lg', 'name' => 'contact-button']) ?>
                            </td>
                        </tr>
                    </table>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>


