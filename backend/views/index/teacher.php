<?php
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<ul class="breadcrumb">
    <li><a href="/">名师团队</a> <span class="divider"></span></li>
    <li class="active">列表</li>
</ul>
<a href="/index/addteacher"><button style="background-color: #367fa9;border-color: #00a7d0;color:whitesmoke">添加名师</button></a><br><br>
<div class="container-fluid">
    <div class="row-fluid">
        <?= GridView::widget([
            'dataProvider' => $data,
            'emptyText' => '暂无数据！',
            //'layout' => "{items}\n{pager}",
            'columns' => [
                [
                    'label' => 'id',
                    'attribute' => 'id',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->id;
                    },
                ],
                [
                    'label' => '名字',
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->name;
                    },
                ],
                [
                    'label' => '头像',
                    'attribute' => 'headimg',
                    'format' => 'raw',
                    'value' => function($data) {
                        return "<img style='width:80px;height:80px;' src='".Yii::$app->params['img_url'].'/'.$data->headimgurl."'>";
                    },
                ],
                [
                    'label' => '学校',
                    'attribute' => 'college',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->college;
                    },
                ],
                [
                    'label' => '学位',
                    'attribute' => 'degree',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->degree;
                    },
                ],
                [
                    'label' => '个人经历',
                    'attribute' => 'description',
                    'format' => 'raw',
                    'value' => function($data) {
                        return mb_substr($data->description,0,20,'utf-8');
                    },
                ],
                [
                    'label' => '操作',
                    'attribute' => '',
                    'format' => 'raw',
                    'value' => function($data) {
                        return  html::a('<span  onclick="return confirm(\'确定删除这条么\')" class="label label-info" title="删除"><i class="fa fa-info-circle fa-lg"></i></span>', Url::to(['index/delteacher', 'id' => $data->id])) . '&nbsp;&nbsp;' .
                        html::a('<span class="label label-primary" title="编辑"><i class="fa fa-cog fa-lg"></i></span>', Url::to(['index/upteacher', 'id' => $data->id]));
                    },
                ],
            ]])?>


    </div>

</div>
