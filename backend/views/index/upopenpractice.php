<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\file\FileInput;
?>
<script src="/kindeditor/kindeditor-min.js"></script>
<script src="/kindeditor/lang/zh_CN.js"></script>
<script src="/js/jquery-1.7.1.min.js"></script>
<link href="/kindeditor/themes/default/default.css">

<script>
    var editor;
    KindEditor.ready(function(K)
    {
        editor = K.create("#openpractice-content",{
            height : "350px",
            allowFileManager:true
        })
    })
</script>
<style type="text/css">
    .select{width:150px;}
</style>


<?php
$form = ActiveForm::begin([
    'options' => [
        'class' => 'form-signin',
        'id' => 'form2',
        'name' => 'form1',
        'enctype' => 'multipart/form-data'
        // 'onSubmit' => 'return msg_form(this)'
    ]
]);
?>

<div class="container-fluid">
    <div class="row-fluid">

        <div class="well">
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane active in" id="home">

                    <table border="0" cellpadding="0" cellspacing="0" id="table">
                        <tr>
                            <td width="30" height="60">&nbsp;</td>
                            <td height="25" width="90%">
                                <?= FileInput::widget([
                                    'name' => 'img',
                                    'id'   => 'img',
                                    'options'=>[
                                        'multiple'=>true
                                    ],
                                    'pluginOptions' => [
                                        'showUpload' => false,
                                        'showRemove' => false,
                                        'initialPreview'=> [
                                            "<img src='" . Yii::$app->params['img_url'] . '/'.$model->img . "' style='width: 100%; height: 160px;' class='file-preview-image' alt='picture' title='picture'>",
                                        ],
                                    ]
                                ]);
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td width="30" height="60">&nbsp;</td>
                            <td height="25" width="90%">
                                <?= FileInput::widget([
                                    'name' => 'img_big',
                                    'id'   => 'img_big',
                                    'options'=>[
                                        'multiple'=>true
                                    ],
                                    'pluginOptions' => [
                                        'showUpload' => false,
                                        'showRemove' => false,
                                        'initialPreview'=> [
                                            "<img src='" . Yii::$app->params['img_url'] .  '/'.$model->img_big . "' style='width: 100%; height: 160px;' class='file-preview-image' alt='picture' title='picture'>",
                                        ],
                                    ]
                                ]);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="30" height="60">&nbsp;</td>
                            <td height="25" width="90%"><?php echo $form->field($model, 'title')->input('text');?></td>
                        </tr>
                        <tr>
                            <td width="30" height="60">&nbsp;</td>
                            <td height="25" width="90%"><?php echo $form->field($model, 'sort')->input('text');?></td>
                        </tr>
                        <tr>
                            <td width="30" height="60">&nbsp;<label class="control-label">内容</label></td>
                            <td height="100" width="90%"><?= $form->field($model, 'content')->textarea()->label(false); ?></td>
                        </tr>

                        <tr>
                            <td width="30" height="60">&nbsp;</td>
                            <td height="25" width="90%" align="center">
                                <?= Html::submitButton('添加', ['class' => 'btn btn-primary btn-primary-2 btn-lg', 'name' => 'contact-button']) ?>

                            </td>
                        </tr>
                    </table>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>


