<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<style type="text/css">
    .select{width:150px;}
</style>


<?php
$form = ActiveForm::begin([
    'options' => [
        'class' => 'form-signin',
        'id' => 'form2',
        'name' => 'form1',
        // 'onSubmit' => 'return msg_form(this)'
    ]
]);
?>

<div class="container-fluid">
    <div class="row-fluid">

        <div class="well">
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane active in" id="home">

                    <table border="0" cellpadding="0" cellspacing="0" id="table">
                        <tr>
                            <td width="30" height="60">&nbsp;</td>
                            <td height="25" width="90%"><?php echo $form->field($model, 'title')->input('text');?></td>
                        </tr>
                        <tr>
                            <td width="30" height="60">&nbsp;</td>
                            <td height="25" width="90%">
                                <?= Html::submitButton('修改', ['class' => 'btn btn-primary btn-primary-2 btn-lg', 'name' => 'contact-button']) ?>

                            </td>
                        </tr>
                    </table>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>


