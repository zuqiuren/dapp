<?php
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<ul class="breadcrumb">
    <li><a href="/">夏令营</a> <span class="divider"></span></li>
    <li class="active">列表</li>
</ul>
<a href="/index/addsummer"><button style="background-color: #367fa9;border-color: #00a7d0;color:whitesmoke">添加夏令营</button></a><br><br>
<div class="container-fluid">
    <div class="row-fluid">
        <?= GridView::widget([
            'dataProvider' => $data,
            'emptyText' => '暂无数据！',
            //'layout' => "{items}\n{pager}",
            'columns' => [
                [
                    'label' => 'id',
                    'attribute' => 'id',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->id;
                    },
                ],
                [
                    'label' => 'img',
                    'attribute' => 'img',
                    'format' => 'raw',
                    'value' => function($data) {
                        return "<img style='width:80px;height:80px;' src='".Yii::$app->params['img_url'].'/'.$data->img."'>";
                    },
                ],
                [
                    'label' => '标题',
                    'attribute' => 'title',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->title;
                    },
                ],
                [
                    'label' => '描述',
                    'attribute' => 'content',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->content;
                    },
                ],
                [
                    'label' => '优惠价',
                    'attribute' => 'price',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->price;
                    },
                ],
                [
                    'label' => '适合对象',
                    'attribute' => 'object',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->object;
                    },
                ],
                [
                    'label' => '营地所在',
                    'attribute' => 'city',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->city;
                    },
                ],
                [
                    'label' => '营期',
                    'attribute' => 'period',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->period;
                    },
                ],
                [
                    'label' => '阵营',
                    'attribute' => 'pid',
                    'format' => 'raw',
                    'value' => function($data) {
                        switch($data->pid)
                        {
                            case 1:
                                return '国内研学';
                            break;
                            case 2:
                                return '国外研学';
                            break;
                        }
                    },
                ],
                [
                    'label' => '操作',
                    'attribute' => '',
                    'format' => 'raw',
                    'value' => function($data) {
                        return  html::a('<span  onclick="return confirm(\'确定删除这条么\')" class="label label-info" title="删除"><i class="fa fa-info-circle fa-lg"></i></span>', Url::to(['index/delsummer', 'id' => $data->id])) . '&nbsp;&nbsp;' .
                        html::a('<span class="label label-primary" title="编辑"><i class="fa fa-cog fa-lg"></i></span>', Url::to(['index/upsummer', 'id' => $data->id]));
                    },
                ],
            ]])?>


    </div>

</div>
