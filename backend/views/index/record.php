<?php
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<ul class="breadcrumb">
    <li><a href="/">在线咨询</a> <span class="divider"></span></li>
    <li class="active">列表</li>
</ul>

<div class="container-fluid">
    <div class="row-fluid">
        <?= GridView::widget([
            'dataProvider' => $data,
            'emptyText' => '暂无数据！',
            //'layout' => "{items}\n{pager}",
            'columns' => [
                [
                    'label' => 'id',
                    'attribute' => 'id',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->id;
                    },
                ],
                [
                    'label' => 'name',
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->name;
                    },
                ],
                [
                    'label' => '年级',
                    'attribute' => 'grade',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->grade;
                    },
                ],
                [
                    'label' => '咨询课程',
                    'attribute' => 'course',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->course;
                    },
                ],
                [
                    'label' => '电话号码',
                    'attribute' => 'phone',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->phone;
                    },
                ],
                [
                    'label' => '留言时间',
                    'attribute' => 'datetime',
                    'format' => 'raw',
                    'value' => function($data) {
                        return $data->datetime;
                    },
                ]
            ]])?>


    </div>

</div>
