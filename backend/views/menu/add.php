<?php
use yii\bootstrap\ActiveForm;
use backend\models\Menu;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
<script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/js/myForm.js" type="text/javascript"></script>
<style>
    #menu-is_show .radio{float:left;}
    #menu-is_show .radio:nth-of-type(2){margin-top: 10px;}
</style>

<ul class="breadcrumb">
	<li>
		<a href="/">首页</a>
		<span class="divider">/</span>
	</li>
	<li class="active">添加菜单</li>
</ul>

<?php
    $form = ActiveForm::begin([
        'id' => 'active-form',
        'action' => '/menu/add',
        'options' => [
            'class' => 'form-signin',
            'enctype' => 'multipart/form-data'
        ],
    ]);

?>

<div class="container-fluid">
	<div class="row-fluid">

		<div class="well">
            <label class="control-label">所属系统</label>
            <div>
                <?php
                echo Html::activeDropDownList($model, 'channel', ArrayHelper::map([ 0 => ['id' => 'admin', 'name' => '后台管理系统'],  1 => ['id' => 'yunying', 'name' => '运营管理系统'], 2 => ['id' => 'config', 'name' => '配置后台'], 3 => ['id' => 'article', 'name' => '官网后台']],'id', 'name'), ['style' => 'width: 220px;height: 40px;border-radius: 5px;box-shadow: 0 0 5px #ccc;position: relative;']);
                ?>

            </div>
        <label class="control-label">所属分类</label>
            <div>
                <?php
                 echo Html::activeDropDownList($model, 'parent_id', ArrayHelper::map($menu,'id', 'name'), ['style' => 'width: 220px;height: 40px;border-radius: 5px;box-shadow: 0 0 5px #ccc;position: relative;']);
                ?>
            </div>
            <div id='name'><?= $form->field($model,'name')->input('text',['style'=>'width: 220px;height: 30px;']); ?></div>

            <div id='url'><?= $form->field($model,'url')->input('text',['style'=>'width: 220px;height: 30px;']); ?></div>
            <div id='weight'><?= $form->field($model,'weight')->input('text',['style'=>'width: 220px;height: 30px;']); ?></div>
            <div id='display'><?= $form->field($model,'is_show')->radioList(Menu::display(),[]); ?></div>
        </div>

        <div class="btn-toolbar">
            <button class="btn btn-primary" name="button" value="tj">
				
				<i class="icon-save"></i>保存</button>
            <a href="/menu/list" data-toggle="modal" class="btn">取消</a>
            <div class="btn-group"></div>
        </div>
        	 
    </div>        
</div>

<?php ActiveForm::end(); ?>
<div class="modal small hide fade" id="message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">提示</h3>
    </div>
    <div class="modal-body">
        <p class="error-text"><i class="icon-warning-sign modal-icon"></i><span>删除成功</span></p>
    </div>
    <div class="modal-footer">
         <button class="btn cancel" data-dismiss="modal" aria-hidden="true">确定</button>
    </div>
</div>

<script>
$(function(){
	<?php if(Yii::$app->session->hasFlash('message')):?>
    $('#message').modal('show');
    $('.error-text span').html('<?php echo Yii::$app->session->getFlash('message');?>');
	<?php endif;?>
});
$("#parentid").change(function() {
    var parent_id = $("#parentid").val();
    if(parent_id == 0){
        $("#mark").show();
        $("#weight").show();
        $("#url").hide();
    }
    else
    {
        $("#mark").hide();
        $("#weight").hide();
        $("#url").show();
    }
});

$(function() {
    var parent_id = $("#parentid").val();
    if(parent_id == 0)
    {
        $("#mark").show();
        $("#weight").show();
        $("#url").hide();
    }else
    {
        $("#mark").hide();
        $("#weight").hide();
        $("#url").show();
    }
});
    $('#menu-channel').change(function()
    {
        $.ajax({
            url: "/menu/ajax-parentid",
            type: "post",
            //dataType: "json",
            data: {'channel': $(this).val()},
            success: function (data) {
                $('#menu-parent_id').html(data);
            }
        })
    })

</script>