<?php
use yii\bootstrap\ActiveForm;
use backend\models\Menu;

?>
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
<link rel="stylesheet" href="/css/font-awesome.css">
<script src="/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="/js/myForm.js" type="text/javascript"></script>
<style>
    #menu-is_show .radio{float:left;}
    #menu-is_show .radio:nth-of-type(2){margin-top: 10px;}
</style>

<ul class="breadcrumb">
	<li>
		<a href="/">首页</a>
		<span class="divider">/</span>
	</li>
	<li class="active">编辑菜单</li>
</ul>

<?php
    $form = ActiveForm::begin([
        'id' => 'active-form',
        'action' => "/menu/edit?id=$id",
        'options' => [
            'class' => 'form-signin',
            'enctype' => 'multipart/form-data'
        ],
    ]);
$channel = Yii::$app->redis->get('channel_'.Yii::$app->user->getIdentity()->id);
?>

<div class="container-fluid">
	<div class="row-fluid">

		<div class="well">
            <div id='parent_id'>
                <?php if($model->parent_id == 0):?>
                    <?= $form->field($model,'parent_id')->dropDownList( [0 => '一级菜单'],['style'=>'width: 220px;height: 34px;']); ?>
                <?php else:?>
                    <?= $form->field($model,'parent_id')->dropDownList( Menu::getParent($model->channel),['style'=>'width: 220px;height: 34px;']); ?>
                <?php endif;?>
            </div>
            <div id='name'><?= $form->field($model,'name')->input('text',['style'=>'width: 220px;height: 30px;']); ?></div>
            <div id=''><?= $form->field($model,'url')->input('text',['style'=>'width: 220px;height: 30px;']); ?></div>
            <div id='weight'><?= $form->field($model,'weight')->input('text',['style'=>'width: 220px;height: 30px;']); ?></div>
            <div id='display'><?= $form->field($model,'is_show')->radioList(Menu::display(),[]); ?></div>
        </div>

        <div class="btn-toolbar">
            <button class="btn btn-primary" name="button" value="tj">
				<i class="icon-save"></i>保存</button>
            <a href="/menu/list" data-toggle="modal" class="btn">取消</a>
            <div class="btn-group"></div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

<div class="modal small hide fade" id="message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">提示</h3>
    </div>
    <div class="modal-body">
        <p class="error-text"><i class="icon-warning-sign modal-icon"></i><span>删除成功</span></p>
    </div>
    <div class="modal-footer">
         <button class="btn cancel" data-dismiss="modal" aria-hidden="true">确定</button>
    </div>
</div>

<script>
$(function(){
	<?php if(Yii::$app->session->hasFlash('message')):?>
    $('#message').modal('show');
    $('.error-text span').html('<?php echo Yii::$app->session->getFlash('message');?>');
	<?php endif;?>
});
$("#parent_id").change(function() {
    var parent_id = $("#parent_id").val();
    if(parent_id == 0){
        $("#mark").show();
        $("#weight").show();
        $("#url").hide();
    }
    else
    {
        $("#mark").hide();
        $("#weight").hide();
        $("#url").show();
    }
});

$(function() {
    var parent_id = $("#parent_id").val();
    if(parent_id == 0)
    {
        $("#mark").show();
        $("#weight").show();
        $("#url").hide();
    }else
    {
        $("#mark").hide();
        $("#weight").hide();
        $("#url").show();
    }
});

</script>