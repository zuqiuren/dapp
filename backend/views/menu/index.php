<?php
use yii\bootstrap\ActiveForm;
?>
<ul class="breadcrumb">
    <li><a href="/">首页</a> <span class="divider">/</span></li>
    <li class="active">城市列表</li>
</ul>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="btn-toolbar">
            <a href="/menu/add"><button class="btn btn-primary" style="margin-bottom:15px;"><i class="icon-plus"></i>新增菜单</button></a>
</div>

<div class="well">
    <table class="table table-bordered table-hover definewidth m10">

        <thead>
            <tr>
                <th>所属系统</th>
                <th>模块名称</th>
                <th>下级菜单</th>
                <th>操作</th>
            </tr>
        </thead>

        <?php foreach ($menu as $key => $value) :?>

            <tr>
                <td><?php if($value['channel'] == 'admin'):?>后台管理系统<?php elseif($value['channel'] == 'yunying'):?>运营管理系统<?php elseif($value['channel'] == 'config'):?>配置后台<?php else:?>官网后台<?php endif;?></td>
                <td><?=$value['name'];?></td>
                <td>
                <?php if(isset($value['find'])):?>
                     <?php foreach ($value['find'] as $val):?>
                     <ol style="margin: 10px 15px;">
                         <?php echo $val['name']."(".$val['url'].")";?>
                         <a href="/menu/edit?id=<?=$val['id']?>">编辑</a>
                     </ol>
                     <?php endforeach;?>
                     <?php endif;?>
                </td>
                <td>
                     <a href="/menu/edit?id=<?=$value['id']?>">编辑</a>
                </td>
            </tr>
        <?php endforeach;?>

    </table>
</div>

<div class="pagination">
    <ul>

    </ul>
</div>

<div class="modal small hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">删除确认</h3>
    </div>
    <div class="modal-body">
        <p class="error-text"><i class="icon-warning-sign modal-icon"></i>您真的要删除码？</p>
    </div>
    <div class="modal-footer">
        <button class="btn cancel" data-dismiss="modal" aria-hidden="true">取消</button>
        <button class="btn btn-danger" data-dismiss="modal">确定</button>
    </div>
</div>

<script type="text/javascript">
//删除城市
$(function(){
    var id = 0;
    $('.btn-danger').click(function(){
        $.getJSON('/city/delete',{'id':id},function(json){
              if(json.status==1){
          	       document.location.reload(true);
              }
              else{
                  alert(json.message);
              }
         });
        return false;
    });
    
    $('.del').click(function(){
        id =$(this).attr('id');
    });
    
});
</script>   
<script type="text/javascript">
//还原城市
$(function(){
	   var id = 0;
        $('.btn-danger').click(function(){
            $.getJSON('/city/recovery',{'id':id},function(json){
                  if(json.status==1){
              	       document.location.reload(true);
                  }
                  else{
                      alert(json.message);
                  }
             });
            return false;
        });

        $('.recovery').click(function(){
            $("#myModalLabel").html("还原确认");
            $(".error-text").html("<i class='icon-warning-sign modal-icon'></i>您真的要还原码？");
            id =$(this).attr('id');
        });
});
</script>                            
