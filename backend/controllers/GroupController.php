<?php
namespace backend\controllers;

use Yii;

use backend\models\User;
use backend\controllers\BaseController;
use backend\models\UserRegister;
use backend\models\Group;
use backend\models\Menu;
use yii\web\Controller;

class GroupController extends BaseController
{
    /**
     * 分组管理
     * @author  li
     * @since   2015-07-29
     */
    public function actionList()
    {   
        //获取分组信息
        $group = Group::find()->asArray()->all();
        //获取用户列表
        $user = User::find()->where(['status'=>10])->asArray()->all();
        //匹配分组下用户
        foreach ($group as $key=>$value) {
            foreach ($user as $val) {
                if($val['gid']==$value['id'])
                {
                    $group[$key]['username'][] = $val['username'];
                }
            }
        }
        
        return $this->render('index',['group'=>$group]);
    }
    
    /**
     * 添加分组
     * @author  li
     * @since   2015-07-29
     * */
    public function actionAdd()
    {
        $model = new Group();
        //获取一级分组
        $menuafirst = Menu::find()->where(['parent_id'=>0])->andWhere("")->asArray()->all();
        foreach ($menuafirst as $key=>$value) {
            $ids[] = $value['id'];
            $menuall[$value['id']] = $value; 
        }
        //获取二级分组
        $menussecond = Menu::find()->where(['parent_id'=>$ids])->asArray()->all();
        
        foreach ($menussecond as $key=>$value) {
            $menuall[$value['parent_id']]['find'][] = $value;
        }
        if($model->load(Yii::$app->request->post()))
        {   
            $add = $model->userGroupAdd();
            if($add)
            {
                $username = Yii::$app->user->getIdentity()->username;
                $channel = Yii::$app->request->post('Group')['channel'];
                $channel = implode(',', $channel);
                $this->addlog($username, '给系统'. $channel .'添加了组，组名为'.trim(Yii::$app->request->post('Group')['name']));
                return $this->redirect('/group/list');
            }
        }
        return $this->render('add',[
            'model'  => $model,
            'module' => $menuall 
        ]);
    }
    
    /**
     * 分组信息修改
     * @author  li
     * @since   2015-07-29
     * */
    public function actionEdit() {
        $model = new Group();
        $id = intval(Yii::$app->request->get('id',0));
        $model = $model->find()->where(['id'=>$id])->one();
        $channel = $model->channel;
        $ids[] = '';
        if($id!=0)
        {
            if($model->load(Yii::$app->request->post())) {
                $mid = Yii::$app->request->post('mid');
                $model->mid = implode(',', $mid);
                if($model->save()) {
                    $username = Yii::$app->user->getIdentity()->username;
                    $this->addlog($username, '修改了组，组名为'.trim(Yii::$app->request->post('Group')['name']));
                    return $this->redirect("/group/list");
                }
            }
            
            $model['mid'] = explode(",", $model['mid']);

            $menuafirst = Menu::find()->where("parent_id=0 and find_in_set(channel, '$channel')")->asArray()->all();
            //var_dump($menuafirst);die;
            foreach ($menuafirst as $key=>$value) {
                $ids[] = $value['id'];
                $menuall[$value['id']] = $value;
            }
            $menussecond = Menu::find()->where(['parent_id'=>$ids])->asArray()->all();
            
            foreach ($menussecond as $key=>$value) {
                $menuall[$value['parent_id']]['find'][$key] = $value;
            }
            unset($menuall[0]);
            return $this->render("edit",[
                    'model'=>$model,
                    'menu'=>$menuall
                ]);
        }
       return $this->redirect("/group/list");
    }
    
    /**
     * 用户删除
     * @author li
     * @since  2015-07-27
     * */
    public function actionDelete()
    {
        $id = intval(Yii::$app->request->get('id',0));
        
        $status = 0;
        $message = '删除失败';
        if(User::delUser($id)){
            $status = 1;
            $message = '删除成功';
        }
        return json_encode([
            'status' => $status,
            'message' => $message
        ]);
    }
      
}
    