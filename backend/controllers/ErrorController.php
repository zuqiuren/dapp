<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use yii\web\HttpException;
class ErrorController extends Controller
{
    
    public function actionIndex()
    {
        $exception = Yii::$app->errorHandler->exception;
        
        //404没找到
        if($exception instanceof  NotFoundHttpException){
            return '404 页面没找到';            
        }
        else if($exception instanceof ErrorException)
        {
            //错误页面
            $params = [
                'to' => 'jing.li@busonline.com',
                'subject' => '程序交易平台 页面报错了',
                'body' => htmlspecialchars(var_export($exception,true)),
            ];
            //压入队列 保存数据库
            // Yii::$app->beanstalk->putInTube('send-mail', $params);
            
            return '500 服务器错误';
        }
    }
}