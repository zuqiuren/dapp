<?php
namespace backend\controllers;

use common\models\Aboutme;
use common\models\Audition;
use common\models\Openpractice;
use common\models\Openpracticeinfo;
use Yii;
use yii\data\ActiveDataProvider;
use common\models\Info;
use common\models\Wonder;
use common\models\ImageThumb;
use common\models\lunbo;
use common\models\Teacherinfo;
use common\models\Teacher;
use common\models\Courseinfo;
use common\models\Course;
use common\models\Pageimg;
use common\models\Summerinfo;
use common\models\Summer;
use common\models\Infostyle;
use common\models\Cooperation;


class IndexController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionInfo()
    {
        $data = new ActiveDataProvider([
            'query' => Info::find(),
            'pagination' => array('pageSize'    => 15),
            'sort' => [
                'defaultOrder' => [
                    'datetime' => SORT_DESC
                ]
            ],
        ]);
        return $this->render('info',[
            'data' => $data
        ]);
    }
    public function actionDelinfo()
    {
        $id = Yii::$app->request->get('id');
        $model = Info::find()->where(['id' => $id])->one();
        if($model)
        {
            $model->delete();
            return $this->redirect('/index/info');
        }
    }
    public function actionAddinfo()
    {
        $model = new Info();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            $model->datetime = date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['info']);
        }
        else
        {
            $pid = Infostyle::find()->all();
            $new_array = [];
            foreach($pid as $k=>$v)
            {
                $new_array[$v->id] = $v->title;
            }
            return $this->render('addinfo',[
                'model' => $model,
                'pid' => $new_array,
            ]);

        }
    }
    public function actionUpinfo()
    {
        $id = Yii::$app->request->get('id');
        $model = Info::find()->where(['id' => $id])->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            $model->update();
            return $this->redirect(['info']);
        }
        else
        {
            $pid = Infostyle::find()->all();
            $new_array = [];
            foreach($pid as $k=>$v)
            {
                $new_array[$v->id] = $v->title;
            }
            return $this->render('upinfo',[
                'model' => $model,
                'pid' => $new_array,
            ]);

        }
    }

    public function actionInfostyle()
    {
        $data = new ActiveDataProvider([
            'query' => Infostyle::find(),
            'pagination' => array('pageSize'    => 15),
            'sort' => [
                'defaultOrder' => [
                    //   'datetime' => SORT_DESC
                ]
            ],
        ]);
        return $this->render('infostyle',[
            'data' => $data
        ]);
    }
    public function actionDelinfostyle()
    {
        $id = Yii::$app->request->get('id');
        $model = Infostyle::find()->where(['id' => $id])->one();
        if($model)
        {
            $model->delete();
            return $this->redirect('/index/infostyle');
        }
    }
    public function actionAddinfostyle()
    {
        $model = new Infostyle();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            $model->save();
            return $this->redirect(['infostyle']);
        }
        else
        {
            return $this->render('addinfostyle',[
                'model' => $model
            ]);

        }
    }
    public function actionUpinfostyle()
    {
        $id = Yii::$app->request->get('id');
        $model = Infostyle::find()->where(['id' => $id])->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            $model->update();
            return $this->redirect(['infostyle']);
        }
        else
        {
            return $this->render('upinfostyle',[
                'model' => $model
            ]);

        }
    }
    public function actionWonder()
    {
        $data = new ActiveDataProvider([
            'query' => Wonder::find(),
            'pagination' => array('pageSize'    => 15),
            'sort' => [
                'defaultOrder' => [
                    'datetime' => SORT_DESC
                ]
            ],
        ]);
        return $this->render('wonder',[
            'data' => $data
        ]);
    }
    public function actionAddwonder()
    {
        $model = new Wonder();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {

            if($_FILES['img']['error']==0)
            {
                $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                $result = ImageThumb::uploadImage($_FILES['img'], $saveFile);
                if ($result['status'] == 1)
                {
                    $model->img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                }
            }
            else
            {
                $model->addError('img','请上传精彩瞬间的图');
            }
            $error = $model->getErrors();
            if(empty($error))
            {
                $model->datetime = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['wonder']);
            }

        }
        else
        {
            return $this->render('addwonder',[
                'model' => $model
            ]);

        }
    }
    public function actionDelwonder()
    {
        $id = Yii::$app->request->get('id');
        $model = Wonder::find()->where(['id' => $id])->one();
        if($model)
        {
            $model->delete();
            return $this->redirect('/index/wonder');
        }
    }
    public function actionUpwonder()
    {
        $id = Yii::$app->request->get('id');
        $model = Wonder::find()->where(['id' => $id])->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if(!empty($_FILES['img']['name']))
            {
                if ($_FILES['img']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['img'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传精彩瞬间的图');
                }
            }
            $error = $model->getErrors();
            if(empty($error))
            {
                $model->update();
                return $this->redirect(['wonder']);
            }
        }
        else
        {
            return $this->render('upwonder',[
                'model' => $model
            ]);

        }
    }
    public function actionLunbo()
    {
        $data = new ActiveDataProvider([
            'query' => Lunbo::find(),
            'pagination' => array('pageSize'    => 15),
            'sort' => [
                'defaultOrder' => [
                    'datetime' => SORT_DESC
                ]
            ],
        ]);
        return $this->render('lunbo',[
            'data' => $data
        ]);
    }
    public function actionAddlunbo()
    {
        $model = new Lunbo();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
//var_dump(Yii::$app->request->post());die;
            if($_FILES['img']['error']==0)
            {
                $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                $result = ImageThumb::uploadImage($_FILES['img'], $saveFile);
                if ($result['status'] == 1)
                {
                    $model->img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                }
            }
            else
            {
                $model->addError('img','请上传精彩瞬间的图');
            }
            $error = $model->getErrors();
            if(empty($error))
            {
                $model->datetime = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['lunbo']);
            }

        }
        else
        {
            return $this->render('addlunbo',[
                'model' => $model
            ]);

        }
    }
    public function actionDellunbo()
    {
        $id = Yii::$app->request->get('id');
        $model = Lunbo::find()->where(['id' => $id])->one();
        if($model)
        {
            $model->delete();
            return $this->redirect('/index/lunbo');
        }
    }
    public function actionUplunbo()
    {
        $id = Yii::$app->request->get('id');
        $model = Lunbo::find()->where(['id' => $id])->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if(!empty($_FILES['img']['name']))
            {
                if ($_FILES['img']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['img'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传轮播');
                }
            }
            $error = $model->getErrors();
            if(empty($error))
            {
                $model->update();
                return $this->redirect(['lunbo']);
            }
        }
        else
        {
            return $this->render('uplunbo',[
                'model' => $model
            ]);

        }
    }
    public function actionTeacherinfo()
    {
        $model = Teacherinfo::find()->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if($model->save())
                echo "<script>alert('修改成功')</script>";
            else
                echo "<script>alert('修改失败')</script>";
        }
        return $this->render('teacherinfo',[
            'model' => $model,
        ]);
    }
    public function actionTeacher()
    {
        $data = new ActiveDataProvider([
            'query' => Teacher::find(),
            'pagination' => array('pageSize'    => 15),
            'sort' => [
                'defaultOrder' => [
                    'datetime' => SORT_DESC
                ]
            ],
        ]);
        return $this->render('teacher',[
            'data' => $data,
        ]);
    }
    public function actionDelteacher()
    {
        $id = Yii::$app->request->get('id');
        $model = Teacher::find()->where(['id' => $id])->one();
        if($model)
        {
            $model->delete();
            return $this->redirect('/index/teacher');
        }
    }
    public function actionAddteacher()
    {
        $model = new Teacher();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
//var_dump(Yii::$app->request->post());die;
            if($_FILES['headimgurl']['error']==0)
            {
                $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                $result = ImageThumb::uploadImage($_FILES['headimgurl'], $saveFile);
                if ($result['status'] == 1)
                {
                    $model->headimgurl = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                }
            }
            else
            {
                $model->addError('img','请上传名师头像');
            }
            $error = $model->getErrors();
            if(empty($error))
            {
                $model->datetime = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['teacher']);
            }

        }
        else
        {
            return $this->render('addteacher',[
                'model' => $model
            ]);

        }
    }
    public function actionUpteacher()
    {
        $id = Yii::$app->request->get('id');
        $model = Teacher::find()->where(['id' => $id])->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if(!empty($_FILES['headimgurl']['name']))
            {
                if ($_FILES['headimgurl']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['headimgurl'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->headimgurl = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('headimgurl', '请上传头像');
                }
            }
            $error = $model->getErrors();
            if(empty($error))
            {
                $model->update();
                return $this->redirect(['teacher']);
            }
        }
        else
        {
            return $this->render('upteacher',[
                'model' => $model
            ]);

        }
    }
    public function actionCourseinfo()
    {
        $model = Courseinfo::find()->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if($model->save())
                echo "<script>alert('修改成功')</script>";
            else
                echo "<script>alert('修改失败')</script>";
        }
        return $this->render('courseinfo',[
            'model' => $model,
        ]);
    }

    public function actionCourse()
    {
        $data = new ActiveDataProvider([
            'query' => Course::find(),
            'pagination' => array('pageSize'    => 15),
            'sort' => [
                'defaultOrder' => [
                    'datetime' => SORT_DESC
                ]
            ],
        ]);
        return $this->render('course',[
            'data' => $data,
        ]);
    }
    public function actionAddcourse()
    {
        $model = new Course();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if($_FILES['img']['error']==0)
            {
                $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                $result = ImageThumb::uploadImage($_FILES['img'], $saveFile);
                if ($result['status'] == 1)
                {
                    $model->img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                }
            }
            else
            {
                $model->addError('img','请上传图片');
            }
            if($_FILES['img_big']['error']==0)
            {
                $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                $result = ImageThumb::uploadImage($_FILES['img_big'], $saveFile);
                if ($result['status'] == 1)
                {
                    $model->img_big = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                }
            }
            else
            {
                $model->addError('img_big','请上传图片');
            }
            $error = $model->getErrors();
            if(empty($error))
            {
                $model->datetime = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['course']);
            }

        }
        else
        {
            return $this->render('addcourse',[
                'model' => $model
            ]);

        }
    }
    public function actionDelcourse()
    {
        $id = Yii::$app->request->get('id');
        $model = Course::find()->where(['id' => $id])->one();
        if($model)
        {
            $model->delete();
            return $this->redirect('/index/course');
        }
    }

    public function actionUpcourse()
    {
        $id = Yii::$app->request->get('id');
        $model = Course::find()->where(['id' => $id])->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if(!empty($_FILES['img']['name']))
            {
                if ($_FILES['img']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['img'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传头像');
                }
            }
            if(!empty($_FILES['img_big']['name']))
            {
                if ($_FILES['img_big']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['img_big'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->img_big = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img_big', '请上传图片');
                }
            }
            $model->update();
            return $this->redirect(['course']);
        }
        else
        {
            return $this->render('upcourse',[
                'model' => $model
            ]);

        }
    }
    public function actionCourseimg()
    {
        $id = Yii::$app->request->get('id');
        $data = new ActiveDataProvider([
            'query' => Pageimg::find()->where("page = $id"),
            'pagination' => array('pageSize'    => 15),
            'sort' => [
                'defaultOrder' => [
                    'datetime' => SORT_DESC
                ]
            ],
        ]);
        return $this->render('courseimg',[
            'data' => $data
        ]);
    }

    public function actionCourseimgadd()
    {
        $model = new Pageimg();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if($_FILES['img']['error']==0)
            {
                $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                $result = ImageThumb::uploadImage($_FILES['img'], $saveFile);
                if ($result['status'] == 1)
                {
                    $model->img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                }
            }
            else
            {
                $model->addError('img','请上传插图');
            }
            $error = $model->getErrors();
            if(empty($error))
            {
                $model->page = Yii::$app->request->get('id');
                $model->datetime = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['courseimg?id='.Yii::$app->request->get('id')]);
            }

        }
        else
        {
            if(Yii::$app->request->get('id') == 1)
                $course = Course::find()->all();
            else
                $course = Summer::find()->all();
            $new_array = [];
            foreach($course as $k=>$v)
            {
                $new_array[$v->id] = $v->title;
            }
            return $this->render('courseimgadd',[
                'model' => $model,
                'course' => $new_array,
            ]);

        }
    }
    public function actionDelimg()
    {
        $id = Yii::$app->request->get('id');
        $model = Pageimg::find()->where(['id' => $id])->one();
        if($model)
        {
            $model->delete();
            return $this->redirect('/index/courseimg?id='.$model->page);
        }
    }

    public function actionUpimg()
    {
        $id = Yii::$app->request->get('id');
        $model = Pageimg::find()->where(['id' => $id])->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if(!empty($_FILES['img']['name']))
            {
                if ($_FILES['img']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['img'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传轮播');
                }
            }
            $error = $model->getErrors();
            if(empty($error))
            {
                $model->update();
                return $this->redirect(['courseimg?id='.$model->page]);
            }
        }
        else
        {
            $course = Course::find()->all();
            $new_array = [];
            foreach($course as $k=>$v)
            {
                $new_array[$v->id] = $v->title;
            }
            return $this->render('upimg',[
                'model' => $model,
                'course' => $new_array,
            ]);
        }
    }

    public function actionSummerinfo()
    {
        $model = Summerinfo::find()->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if($model->save())
                echo "<script>alert('修改成功')</script>";
            else
                echo "<script>alert('修改失败')</script>";
        }
        return $this->render('summerinfo',[
            'model' => $model,
        ]);
    }
    public function actionSummer()
    {
        $data = new ActiveDataProvider([
            'query' => Summer::find(),
            'pagination' => array('pageSize'    => 15),
            'sort' => [
                'defaultOrder' => [
                    'datetime' => SORT_DESC
                ]
            ],
        ]);
        return $this->render('summer',[
            'data' => $data
        ]);
    }

    public function actionAddsummer()
    {
        $model = new Summer();

        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if($_FILES['img']['error']==0)
            {
                $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                $result = ImageThumb::uploadImage($_FILES['img'], $saveFile);
                if ($result['status'] == 1)
                {
                    $model->img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                }
            }
            else
            {
                $model->addError('img','请上传图片');
            }
            if($_FILES['img_big']['error']==0)
            {
                $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                $result = ImageThumb::uploadImage($_FILES['img_big'], $saveFile);
                if ($result['status'] == 1)
                {
                    $model->img_big = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                }
            }
            else
            {
                $model->addError('img_big','请上传图片');
            }
            $error = $model->getErrors();
            if(empty($error))
            {
                $model->datetime = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['summer']);
            }
        }
        else
        {
            return $this->render('addsummer',[
                'model' => $model
            ]);

        }
    }

    public function actionDelsummer()
    {
        $id = Yii::$app->request->get('id');
        $model = Summer::find()->where(['id' => $id])->one();
        if($model)
        {
            $model->delete();
            return $this->redirect('/index/summer');
        }
    }

    public function actionUpsummer()
    {
        $id = Yii::$app->request->get('id');
        $model = Summer::find()->where(['id' => $id])->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if(!empty($_FILES['img']['name']))
            {
                if ($_FILES['img']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['img'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传头像');
                }
            }
            if(!empty($_FILES['img_big']['name']))
            {
                if ($_FILES['img_big']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['img_big'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->img_big = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img_big', '请上传头像');
                }
            }
            $model->update();
            return $this->redirect(['summer']);
        }
        else
        {
            return $this->render('upsummer',[
                'model' => $model
            ]);

        }
    }
    public function actionOpenpracticeinfo()
    {
        $model = Openpracticeinfo::find()->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if($model->save())
                echo "<script>alert('修改成功')</script>";
            else
                echo "<script>alert('修改失败')</script>";
        }
        return $this->render('openpracticeinfo',[
            'model' => $model,
        ]);
    }
    public function actionOpenpractice()
    {
        $data = new ActiveDataProvider([
            'query' => Openpractice::find(),
            'pagination' => array('pageSize'    => 15),
            'sort' => [
                'defaultOrder' => [
                    'datetime' => SORT_DESC
                ]
            ],
        ]);
        return $this->render('openpractice',[
            'data' => $data
        ]);
    }
    public function actionAddopenpractice()
    {
        $model = new Openpractice();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if($_FILES['img']['error']==0)
            {
                $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                $result = ImageThumb::uploadImage($_FILES['img'], $saveFile);
                if ($result['status'] == 1)
                {
                    $model->img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                }
            }
            else
            {
                $model->addError('img','请上传图片');
            }
            if($_FILES['img_big']['error']==0)
            {
                $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                $result = ImageThumb::uploadImage($_FILES['img_big'], $saveFile);
                if ($result['status'] == 1)
                {
                    $model->img_big = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                }
            }
            else
            {
                $model->addError('img_big','请上传图片');
            }
            $error = $model->getErrors();
            if(empty($error))
            {
                $model->datetime = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['openpractice']);
            }
        }
        else
        {
            return $this->render('addopenpractice',[
                'model' => $model
            ]);

        }
    }
    public function actionUpopenpractice()
    {
        $id = Yii::$app->request->get('id');
        $model = Openpractice::find()->where(['id' => $id])->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if(!empty($_FILES['img']['name']))
            {
                if($_FILES['img']['error']==0)
                {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['img'], $saveFile);
                    if ($result['status'] == 1)
                    {
                        $model->img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                }
                else
                {
                    $model->addError('img','请上传图片');
                }
            }
            if(!empty($_FILES['img_big']['name']))
            {
                if($_FILES['img_big']['error']==0)
                {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['img_big'], $saveFile);
                    if ($result['status'] == 1)
                    {
                        $model->img_big = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                }
                else
                {
                    $model->addError('img_big','请上传图片');
                }
            }

            $error = $model->getErrors();
            if(empty($error))
            {
                $model->datetime = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['openpractice']);
            }
        }
        else
        {
            return $this->render('upopenpractice',[
                'model' => $model
            ]);

        }
    }
    public function actionDelopenpractice()
    {
        $id = Yii::$app->request->get('id');
        $model = Openpractice::find()->where(['id' => $id])->one();
        if($model)
        {
            $model->delete();
            return $this->redirect('/index/openpractice');
        }
    }
    public function actionRecord()
    {
        $data = new ActiveDataProvider([
            'query' => Audition::find(),
            'pagination' => array('pageSize'    => 15),
            'sort' => [
                'defaultOrder' => [
                    'datetime' => SORT_DESC
                ]
            ],
        ]);
        return $this->render('record',[
            'data' => $data
        ]);
    }
    public function actionCooperation()
    {
        $model = Cooperation::find()->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if($model->save())
                echo "<script>alert('修改成功')</script>";
            else
                echo "<script>alert('修改失败')</script>";
        }
        return $this->render('cooperation',[
            'model' => $model,
        ]);
    }
    public function actionAboutme()
    {
        $model = Aboutme::find()->one();
        if ( $model->load(Yii::$app->request->post()) && $model->validate() )
        {
            if($model->save())
                echo "<script>alert('修改成功')</script>";
            else
                echo "<script>alert('修改失败')</script>";
        }
        return $this->render('aboutme',[
            'model' => $model,
        ]);
    }
}