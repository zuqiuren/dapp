<?php
namespace backend\controllers;

use PHPUnit\Framework\Exception;
use Yii;
use yii\data\ActiveDataProvider;
use backend\models\ConfigWidgetText;


class WidgetController extends BaseController
{

    /***
     *   表单组件库  文本信息
     */
    public function actionText()
    {
        return $this->render('text',[
        ]);
    }

    public function actionRequestText()
    {
        $model = new ConfigWidgetText();
        $data = $model->getInfo();
        echo json_decode($data);
    }

    public function actionSubRequestText()
    {
        $model = new ConfigWidgetText();
        $post = Yii::$app->request->post();
        if(!isset($post['name']) || !isset($post['check']) || !isset($post['style']) || !isset($post['widget_name']) || !isset($post['widget_remark']))
        {
            echo $this->make_json([], 400, '参数不对');
        }
        try
        {

        }
        catch(Exception $e)
        {

        }
        $model = new ConfigWidgetText();
        $data = $model->getInfo();
        echo $this->make_json($data);
    }

    /***
     *   表单组件库  选择信息
     */
    public function actionChoice()
    {
        return $this->render('choice');
    }

    /***
     *   表单组件库  多级选择
     */
    public function actionMoreChoice()
    {
        return $this->render('morechoice');
    }

    /***
     *   表单组件库  附件信息
     */
    public function actionAttachment()
    {
        return $this->render('attachment');
    }
    /***
     *  表单组件库   时间
     */
    public function actionTime()
    {
        return $this->render('time');
    }
}