<?php
namespace backend\controllers;

use backend\models\Dapp;
use common\models\Paging;
use common\models\ImageThumb;
use backend\models\DappPreview;
use Yii;


/****
 * @author ge date 2019-02-20
 */
class DappController extends BaseController
{
    /****
     * dapp列表
     */
    public function actionList()
    {
        $name = Yii::$app->request->get('app_name');
        $page = Yii::$app->request->get('p', 1);
        $category = Yii::$app->request->get('category', '');
        $is_mobile = Yii::$app->request->get('is_mobile', '');
        $where = 'status = 1';
        $search = '';
        if($name)
        {
            $where .= " and dapp_name LIKE '%$name%'";
            $search .= "&app_name=".$name.'&';
        }
        if($category != '' && $category != 'all')
        {
            $where .= " and category = '$category'";
            $search .= "category=".$category.'&';
        }



        switch($is_mobile)
        {
            case '0':
                $where .= " and is_mobile = '$is_mobile'";
                $search .= "&is_mobile=".$is_mobile.'&';
                break;
            case '1':
                $where .= " and is_mobile >= '$is_mobile'";
                $search .= "&is_mobile=".$is_mobile.'&';
                break;
            default:
                break;
        }

        $pageHtml = '';
        $data = Dapp::getlist($where, 'sort_weight desc', $page, 15);
        $logo = $lunbo = [];
        foreach($data['list'] as $k=>$v)
        {
            $preview = DappPreview::find()->where(['dapp_id' => $v['id']])->one();
            $logo[$k] = $preview['logo_image_url'];
            $lunbo[$k] = $preview['lunbo_image_url'];
        }

        if ($data['totalPage'] > 0) {
            $page_total = $data['totalPage'];

            $pageHtml = Paging::make($page, $page_total, '/dapp/list?'.$search.'p=');
        }
        return $this->render('list', [
            'dapp' => $data['list'],
            'pageHtml' => $pageHtml,
            'logo' => $logo,
            'name' => $name,
            'category' => $category,
            'lunbo' => $lunbo,
            'mobile' => $is_mobile,
        ]);
    }

    public function actionAlterStatus()
    {
        $type = Yii::$app->request->post('type');
        $id = Yii::$app->request->post('id');
        $dapp = Dapp::find()->where(['id' => $id])->one();
        switch($type)
        {
            case 'm':
                if($dapp['is_mobile'] == 0)
                {
                    $dapp['is_mobile'] = 1;
                }
                else
                {
                    $dapp['is_mobile'] = 0;
                }
                break;
            case 'p':
                if($dapp['is_mobile'] == 1)
                {
                    $dapp['is_mobile'] = 2;
                }
                else
                {
                    $dapp['is_mobile'] = 1;
                }
                break;
            case 'l':
                if($dapp['is_lunbo'] == 0)
                {
                    $dapp['is_lunbo'] = 1;
                }
                else
                {
                    $dapp['is_lunbo'] = 0;
                }
                break;
        }

        $dapp->save();
    }

    public function actionAlterWeight()
    {
        $id = Yii::$app->request->post('id');
        $sort = Yii::$app->request->post('sort');
        $dapp = Dapp::find()->where(['id' => $id])->one();
        $dapp['sort_weight'] = $sort;
        $dapp->save();
    }

    public function actionUpload()
    {
        $appid = Yii::$app->request->post('app_id', 0);
        if($appid && isset($_FILES['file']['name']))
        {
            $result = ImageThumb::uploadImage($_FILES['file'], '../../../walletAPI/public/dapp/img/');
            if($result['status'] == 1)
            {
                Yii::$app->getSession()->setFlash('success', "上传成功!");
                $preview = DappPreview::find()->where(['dapp_id' => $appid])->one();
                if($preview)
                {
                    $preview['lunbo_image_url'] = Yii::$app->params['img_url'] . '/dapp/img/' . $result['imageDir'];
                    $preview->save();
                }
                else
                {
                    Yii::$app->getSession()->setFlash('success', "上传失败!");
                }
            }
            else
            {
                Yii::$app->getSession()->setFlash('success', "上传失败!");
            }
        }
        else
        {
            Yii::$app->getSession()->setFlash('success', "上传失败!");
        }

        return $this->redirect(['list']);
    }
}