<?php
namespace backend\controllers;

use Yii;
use Yii\helpers\ArrayHelper;
use backend\controllers\BaseController;
use backend\models\UserForm;
use backend\models\UserRegister;
use common\models\Helper;
use backend\models\User;
use common\models\Paging;
/**
 * Site controller
 */
class UserController extends BaseController
{
    /**
     * 退出
     * @author xi
     * @date 2015-4-18
     */
    public function actionLogin()
    {
        $this->layout = false;
        if (! Yii::$app->user->isGuest) {
            return $this->redirect('/');
        }
        $model = new UserForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->redirect('/');
        } else {
            return $this->render('login', [
                'model' => $model
            ]);
        }
    }

    /**
     * 退出
     * 
     * @author xi
     * @date 2015-4-18
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect('/user/login');
    }

    /**
     * 填加用户
     * @author:xia
     */
    public function actionAdd()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/login');
        }
        $model = new UserRegister(['scenario' => 'add']);
        if ($model->load($post = Yii::$app->request->post())) {
            if($model->validate()) {
            if ($user = $model->signup()) {
                Yii::$app->session->setFlash('message', '添加成功');
                return $this->redirect('/user/list');
            }
            }
        }
        $group = \backend\models\Group::find()->asArray()->all();
        $group = ArrayHelper::map($group,'id','name');

        return $this->render('add', [
            'model' => $model,
            'group' => $group
        ]);
    }

    /**
     * 用户列表
     * 
     * @return json
     * @author xi
     * @date 2015-4-19
     */
    public function actionList()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/user/login');
        }
        
        $pageHtml = '';
        $page = Yii::$app->request->get('p', 1);
        
        $data = User::getlist([], '', $page, 15);
        
        if ($data['totalPage'] > 0) {
            $page_total = $data['totalPage'];
            $pageHtml = Paging::make($page, $page_total, '/user/list?p=');
        }
        return $this->render('index', [
            'user' => $data['list'],
            'pageHtml' => $pageHtml
        ]);
    }

    /**
     * 停用用户
     * @return string
     */
    public function actionDelete()
    {
        $id = intval(Yii::$app->request->get('id', 0));
        
        $status = 0;
        $message = '停用失败';
        $user = User::findOne($id);
        if (User::changeStatus($id, 0)) {
            echo "<script>alert('停用成功');window.location.href='/user/list'</script>";
        }
        else
        {
            echo "<script>alert('停用失败');window.location.href='/user/list'</script>";
        }
        //sleep(2);
        //return $this->redirect(['user/list']);
//        return json_encode([
//            'status' => $status,
//            'message' => $message
//        ]);
    }

    /**
     * 启用用户
     * @return string
     */
    public function actionStart()
    {
        $id = intval(Yii::$app->request->get('id', 0));
        
        $status = 0;
        $message = '启用失败';
        $user = User::findOne($id);
        if (User::changeStatus($id, 10)) {
            echo "<script>alert('启动成功');window.location.href='/user/list'</script>";
        }
        else
        {
            echo "<script>alert('启动失败');window.location.href='/user/list'</script>";
        }
//        return json_encode([
//            'status' => $status,
//            'message' => $message
//        ]);
    }

    /**
     * 修改
     * 
     * @author xi
     * @date 2015-4-19
     */
    public function actionEdit()
    {
        //phpinfo();
        $id = Yii::$app->request->get('id', 1);
        $model = new UserRegister();
        
        $data = User::findOne([
            'id' => $id
        ]);
        if (! $data) {
            return $this->redirect('/user/list');
        }
        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                if ($user = $model->modify($id)) {
                    Yii::$app->session->setFlash('message', '修改成功');
                    return $this->redirect('/user/edit?id=' . $id);
                }
            }
            print_r($model->getErrors());die;
        }
        $group = \backend\models\Group::find()->asArray()->all();
        $group = ArrayHelper::map($group, 'id', 'name');

        $model->gid      = $data->gid;
        $model->username = $data->username;
        $model->email    = $data->email;
        
        return $this->render('edit', [
            'model' => $model,
            'id'    => $id,
            'group' => $group
        ]);
    }

    public function actionAlterPassword()
    {
        $model = new UserRegister();
        if ($model->load($post = Yii::$app->request->post())) {
            if($post['UserRegister']['password'] != $post['password_new'])
            {
                Yii::$app->session->setFlash('message', '2次密码不相同');
                return $this->redirect(-1);
            }
            if ($user = $model->modifyPassword()) {
                Yii::$app->session->setFlash('message', '修改成功');
                return $this->redirect('/index/index');
            }
        }
        return $this->render('alter-password', [
            'model' => $model,
        ]);
    }

    public function actionChannel()
    {
       // var_dump(Yii::$app->request->post());die;
        $channel = Yii::$app->request->post('channel');
        Yii::$app->redis->set('channel_'.Yii::$app->user->getIdentity()->id, $channel);
        echo json_encode([]);
    }
}
