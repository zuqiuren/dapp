<?php
namespace backend\controllers;

use Yii;
use backend\models\Admin;
use backend\models\LoginForm;
use common\models\Helper;
use backend\models\UserLog;
use yii\base\Action;
use yii\web\Controller;

class LoginController extends BaseController
{

    /**
     * 登录到首页
     * @author :xia
     */
    public function actionAdmin()
    {
        return $this->render('index');
    }

    /**
     * 判断用户是否登录
     * @return \yii\web\Response|string
     */
    public function actionLogin()
    {

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $username = Yii::$app->user->getIdentity()->username;
            $this->addlog($username, '用户登录！');
            return $this->redirect('/admin/index');
        } else {
            
            return $this->renderPartial('login', [
                'model' => $model
            ]);
        }
    }

    /**
     * 退出系统
     * LoginOut
     */
    public function actionLogout()
    {
        //$username = Yii::$app->user->getIdentity()->username;
        Yii::$app->user->logout();
        //$this->addlog($username, '用户退出登录！');
        return $this->redirect('/user/login');
    }
}