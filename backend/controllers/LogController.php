<?php
namespace backend\controllers;

use backend\models\UserLog;
use Yii;
use Yii\helpers\ArrayHelper;
use backend\controllers\BaseController;
use backend\models\UserForm;
use backend\models\Menu;
use common\models\Helper;
use backend\models\User;
use common\models\Paging;
/**
 * Site controller
 */
class LogController extends BaseController
{
    /**
     * 退出
     * @author xi
     * @date 2015-4-18
     */
    public function actionList()
    {
        $page     = intval(Yii::$app->request->get('page',1));
        $pageSize = 15;
        $order = "datetime desc";
        $search['url'] = Yii::$app->request->get('url','');
        $search['username'] = Yii::$app->request->get('username','');
        $where = '';
        if($search['url'])
        {
            $where = "url='".$search['url']."' and";
        }

        if($search['username'])
        {
            $where .= " name='".$search['username']."'";
        }
        $where = trim($where, 'and');
        $data = UserLog::getList($where,$order,$page,$pageSize);
        $pageHtml = '';
        if($data['totalPage']>1){
            $se = http_build_query($search);
            $pageHtml = Paging::make($page, $data['totalPage'], '?'.$se.'&page=');
        }
        $menu = Menu::find()->asArray()->where('parent_id > 0')->all();
        //var_dump($search['url']);die;
        return $this->render('list',[
            'pageHtml' => $pageHtml,
            'data' => $data,
            'search'    => $search,
            'menu'     =>  $menu
        ]);
    }
}