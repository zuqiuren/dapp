<?php
namespace backend\controllers;

use backend\models\ConfigForm;
use backend\models\ConfigFormList;
use backend\models\ConfigStencil;
use backend\models\WidgetChoice;
use backend\models\WidgetChoiceList;
use backend\models\WidgetChoiceMore;
use backend\models\WidgetFile;
use backend\models\WidgetText;
use backend\models\WidgetTime;
use backend\models\ConfigStencilType;

use Yii;


/****
 * @author ge date 2019-01-23
 */
class ConfigController extends \deepziyu\yii\rest\Controller
{
    /****
     * 表单组件库 文本信息样式
     */
    public function actionRequestText()
    {
        $model = WidgetText::find()->one();
        if(!$model)
        {
            $model = [];
        }
        return $model;
    }
    /****
     * 表单组件库 文本信息提交
     */
    public function actionSubRequestText()
    {
        $post['WidgetText'] = Yii::$app->request->get();
        $model = WidgetText::find()->one();
        if(!$model)
        {
            $model = new WidgetText();
            $model->create_time = date('Y-m-d H:i:s');
            $model->load($post);
            if(!$model->save())
            {
                Yii::$app->response->statusCode = 400;
                Yii::$app->response->statusText = json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE);
                return [];
            }
        }
        else
        {
            $model->update_time = date('Y-m-d H:i:s');
            $model->load($post);
            if(!$model->save())
            {
                Yii::$app->response->statusCode = 400;
                Yii::$app->response->statusText = json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE);
                return [];
            }
        }
        return [];
    }

    /****
     * 表单组件库 选择信息样式
     */
    public function actionChoice()
    {
        $model = WidgetChoice::find()->one();
        if(!$model)
        {
            $model = [];
        }
        return $model;
    }
    /****
     * 表单组件库 选择信息提交
     */
    public function actionSubChoice()
    {
        $post['WidgetChoice'] = Yii::$app->request->get();
        $model = WidgetChoice::find()->one();
        if(!$model)
        {
            $model = new WidgetChoice();
            $model->create_time = date('Y-m-d H:i:s');
            $model->load($post);
            if(!$model->save())
            {
                Yii::$app->response->statusCode = 400;
                Yii::$app->response->statusText = json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE);
                return [];
            }
        }
        else
        {
            $model->update_time = date('Y-m-d H:i:s');
            $model->load($post);
            if(!$model->save())
            {
                Yii::$app->response->statusCode = 400;
                Yii::$app->response->statusText = json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE);
                return [];
            }
        }
        return [];
    }

    /****
     * 表单组件库 多级选择样式 一级分类列表
     */
    public function actionChoiceMoreFirst()
    {
        $base = WidgetChoiceMore::find()->one();
        $arr = [];
        if($base)
        {
            $arr['base'] = $base;
            $arr['list'] = [];
            $arr['list'] = WidgetChoiceList::find()->asArray()->where(['pid' => 0])->all();
        }
        return $arr;
    }

    /****
     * 表单组件库 多级选择样式 2到4级分级样式
     */

    public function actionChoiceMoreS()
    {
        $id = Yii::$app->request->get('id');
        if(!$id)
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '参数错误';
            return [];
        }
        $arr = WidgetChoiceList::find()->asArray()->where(['pid' => $id])->all();
        return $arr;
    }

    /***
     * @param $list
     * @param int $pid
     * @return 备选方案, 递归一次查找
     */
    public function getChoiceList($list, $pid = 0)
    {
        $tree = [];
        foreach($list as $k => $v)
        {
            if($v['pid'] == $pid)
            {
                $v['child'] = self::getChoiceList($list, $v['id']);
                $tree[] = $v;
            }
        }
        return $tree;
    }
    /****
     * 表单组件库 选择信息提交
     */
    public function actionSubChoiceMore()
    {
        $post = Yii::$app->request->get();
        if(empty($post['name']) || empty($post['widget_name']) || empty($post['widget_remark']))
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '参数错误';
            return [];
        }
        $item = json_decode($post['item']);
        $model = WidgetChoiceMore::find()->one();
        if(!$model)
        {
            $model = new WidgetChoiceMore();
            $model->name = $post['name'];
            $model->content = Yii::$app->request->post('content', '');
            $model->must = Yii::$app->request->post('must', 0);
            $model->widget_name = $post['widget_name'];
            $model->widget_remark = $post['widget_remark'];
            $model->create_time = date('Y-m-d H:i:s');
            if(!$model->save())
            {
                Yii::$app->response->statusCode = 400;
                Yii::$app->response->statusText = json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE);
                return [];
            }
        }
        else
        {
            $model->name = $post['name'];
            $model->content = Yii::$app->request->post('content', '');
            $model->must = Yii::$app->request->post('must', '');
            $model->widget_name = $post['widget_name'];
            $model->widget_remark = $post['widget_remark'];
            $model->update_time = date('Y-m-d H:i:s');
            $model->load($post);
            if(!$model->save())
            {
                Yii::$app->response->statusCode = 400;
                Yii::$app->response->statusText = json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE);
                return [];
            }
        }
        Yii::$app->db->createCommand('truncate table pro_config_widget_choice_list')->execute();

        if(!empty($item->item))
        {
            foreach($item->item as $v)
            {
                $list = new WidgetChoiceList();
                $list->other = $v->other;
                $list->other_title = $v->other_title;
                $list->type = $v->type;
                $list->content = $v->content;
                $list->pid = 0;
                $list->mid = $model->id;
                $list->save();
                $item_id_first = $list->id;
                if(!empty($v->item))
                {
                    foreach($v->item as $s)
                    {
                        $list = new WidgetChoiceList();
                        $list->other = $s->other;
                        $list->other_title = $s->other_title;
                        $list->type = $s->type;
                        $list->content = $s->content;
                        $list->pid = $item_id_first;
                        $list->mid = $model->id;
                        $list->save();
                        $item_id_second = $list->id;
                        if(!empty($s->item))
                        {
                            foreach($s->item as $t)
                            {
                                $list = new WidgetChoiceList();
                                $list->other = $t->other;
                                $list->other_title = $t->other_title;
                                $list->type = $t->type;
                                $list->content = $t->content;
                                $list->pid = $item_id_second;
                                $list->mid = $model->id;
                                $list->save();
                                $item_id_three = $list->id;
                                if(!empty($t->item))
                                {
                                    foreach($t->item as $f)
                                    {
                                        $list = new WidgetChoiceList();
                                        $list->other = $f->other;
                                        $list->other_title = $f->other_title;
                                        $list->type = $f->type;
                                        $list->content = $f->content;
                                        $list->pid = $item_id_three;
                                        $list->mid = $model->id;
                                        $list->save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return [];
    }

    /***
     * @return 表单组件库 时间查询
     */
    public function actionTime()
    {
        $arr = WidgetTime::find()->asArray()->one();
        return $arr;
    }

    public function actionSubTime()
    {
        $model = WidgetTime::find()->asArray()->one();
        $post['WidgetTime'] = Yii::$app->request->post();
        if(!$model)
        {
            $model = new WidgetTime();
        }
        $model->load($post);
        if(!$model->save())
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE);
            return [];
        }
        return [];
    }

    /***
     *  表单组件库  附件查询
     */
    public function actionFile()
    {
        $arr = WidgetFile::find()->asArray()->one();
        return $arr;
    }

    /***
     * @return 附件信息提交
     */
    public function actionSubFile()
    {
        $model = WidgetFile::find()->asArray()->one();
        $post['WidgetFile'] = Yii::$app->request->post();
        if(!$model)
        {
            $model = new WidgetFile();
        }
        $model->load($post);
        if(!$model->save())
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE);
            return [];
        }
        return [];
    }

    /***
     *  组件列表
     */
    public function actionList()
    {
        $arr = [];
        $text = WidgetText::find()->select('id,widget_name,widget_remark,create_time,author')->asArray()->one();
        $choice = WidgetChoice::find()->select('id,widget_name,widget_remark,create_time,author')->asArray()->one();
        $choice_more = WidgetChoiceMore::find()->select('id,widget_name,widget_remark,create_time,author')->asArray()->one();
        $time = WidgetTime::find()->select('id,widget_name,widget_remark,create_time,author')->asArray()->one();
        $file = WidgetFile::find()->select('id,widget_name,widget_remark,create_time,author')->asArray()->one();
        if($text)
        {
            $text['type'] = 1;
            array_push($arr, $text);
        }
        if($choice)
        {
            $choice['type'] = 2;
            array_push($arr, $choice);
        }
        if($choice_more)
        {
            $choice_more['type'] = 3;
            array_push($arr, $choice_more);
        }
        if($time)
        {
            $time['type'] = 4;
            array_push($arr, $time);
        }
        if($file)
        {
            $file['type'] = 4;
            array_push($arr, $file);
        }
        return $arr;
    }

    /***
     * 组件列表修改（只能改组件名称，组件备注）
     */
    public function actionSubList()
    {
        $post = Yii::$app->request->get();
        if(empty($post['type']) || empty($post['widget_name']) || !isset($post['widget_remark']))
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '参数错误';
            return [];
        }
        switch($post['type'])
        {
            case '1':
                $model_name = 'backend\models\WidgetText';
                break;
            case '2':
                $model_name = 'backend\models\WidgetChoice';
                break;
            case '3':
                $model_name = 'backend\models\WidgetChoiceMore';
                break;
            case '4':
                $model_name = 'backend\models\WidgetTime';
                break;
            case '5':
                $model_name = 'backend\models\WidgetFile';
                break;
            default:
                Yii::$app->response->statusCode = 400;
                Yii::$app->response->statusText = '非法请求';
                return [];
        }
        $model = $model_name::find()->one();
        if(!$model)
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '非法请求';
            return [];
        }

        $model->widget_name = $post['widget_name'];
        $model->widget_remark = $post['widget_remark'];
        if(!$model->save())
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE);
            return [];
        }

        return [];
    }

    /***
     * @return 表单选择组件
     */
    public function actionFormWidgetChoice()
    {
        $type = Yii::$app->request->get('type', 0);
        switch($type)
        {
            case '1':
                $model_name = 'backend\models\WidgetText';
                break;
            case '2':
                $model_name = 'backend\models\WidgetChoice';
                break;
            case '3':
                $model_name = 'backend\models\WidgetChoiceMore';
                break;
            case '4':
                $model_name = 'backend\models\WidgetTime';
                break;
            case '5':
                $model_name = 'backend\models\WidgetFile';
                break;
            default:
                Yii::$app->response->statusCode = 400;
                Yii::$app->response->statusText = '非法请求';
                return [];
        }
        $model = $model_name::find()->one();
        if(!$model)
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '请设置该组件';
            return [];
        }
        return $model;
    }

    /***
     * 提交表单
     */
    public function actionSubForm()
    {
        $type = Yii::$app->request->get('type', 0);
        $widget = Yii::$app->request->get('widget', 0);
        $form_id = Yii::$app->request->get('form_id', 0);
        if(!in_array($type, [1,2,3,4,5]) || !in_array($widget, [1,2,3,4]) || !($form_id >= 0))
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '非法请求';
            return [];
        }
        if($form_id > 0)
        {
            $model = ConfigForm::find()->where(['id' => $form_id])->one();
            $model->update_time = date('Y-m-d H:i:s');
        }
        else
        {
            $model = new ConfigForm();
            $model->create_time = date('Y-m-d H:i:s');
        }
        $model->operator = Yii::$app->user->getIdentity()->username;
        $model->rate = Yii::$app->request->post('rate', '');
        $model->least = Yii::$app->request->post('least', '');
        $model->most = Yii::$app->request->post('most', '');
        $model->time_limit = Yii::$app->request->post('time_limit', '');
        $model->mortgage = Yii::$app->request->post('mortgage', '');
        $model->loan_type = Yii::$app->request->post('loan_type', '');
        $model->lending_time = Yii::$app->request->post('lending_time', '');
        $model->status = Yii::$app->request->post('status', '');
        $model->type = Yii::$app->request->post('type', '');
        if(!$model->save())
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE);
            return [];
        }
        $widget = explode(',', $widget);
        if(!empty($widget))
        {
            if($form_id == 0)
            {
                $form_id = $model->id;
            }
            else
            {
                $list = new ConfigFormList();
                $list->where(['form_id' => $form_id])->delete();
            }
            foreach($widget as $v)
            {
                $list = new ConfigFormList();
                $list->form_id = $form_id;
                $list->widget = $v;
                if(!$list->save())
                {
                    Yii::$app->response->statusCode = 400;
                    Yii::$app->response->statusText = json_encode($list->getErrors(), JSON_UNESCAPED_UNICODE);
                    return [];
                }
            }
        }

        return [];
    }

    /***
     * @return 产品模版分类
     */
    public function actionStencilType()
    {
        $type = ConfigStencilType::find()->all();
        return $type;
    }

    /***
     * 添加修改模版分类
     */
    public function actionSubStencilType()
    {
        $id = Yii::$app->request->post('id');
        $name = Yii::$app->request->post('name','');
        if(!$name)
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '非法请求';
            return [];
        }
        if($id)
        {
            $model = ConfigStencilType::find()->where(['id' => $id])->one();
        }
        else
        {
            $model = new ConfigStencilType();
        }
        $model->name = $name;
        if(!$model->save())
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE);
            return [];
        }
    }

    /***
     * @return 删除模版分类
     */
    public function actionDelStencilType()
    {
        $id = Yii::$app->request->post('id');
        if(!$id)
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '非法请求';
            return [];
        }
        $model = new ConfigStencilType();
        $model->where(['id' => $id])->delete();
        return [];
    }

    /***
     *  添加模版页面选择表单列表
     */
    public function actionStencilForm()
    {
        $type = Yii::$app->request->get('type', 0);
        if(!in_array($type,[1,2,3,4]))
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '非法请求';
            return [];
        }
        $list = ConfigForm::find()->select('name, id')->where(['status' => 1, 'type' => $type])->all();
        return $list;
    }

    /***
     * 添加模版页面点击列表预览
     */
    public function actionStencilFormDetail()
    {
        $id = Yii::$app->request->get('id', 0);
        $type = Yii::$app->request->get('type', 0);
        if(!in_array($type,[1,2,3,4,5]))
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '非法请求';
            return [];
        }
        $form = ConfigForm::find()->where(['id' => $id])->one();
        if($form->type != $type)
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '非法请求';
            return [];
        }
        $list = ConfigFormList::find()->where(['form_id' => $id])->all();
        $text = $this->actionRequestText();
        $text['style'] = 1;
        $choice = $this->actionChoice();
        $choice['style'] = 2;
        $choice_more = $this->actionChoiceMoreFirst();
        $choice_more['style'] = 3;
        $time = $this->actionTime();
        $time['style'] = 4;
        $file = $this->actionFile();
        $file['style'] = 5;
        $arr = [];
        foreach($list as $v)
        {
            switch($v->type)
            {
                case '1':
                    array_push($arr, $text);
                    break;
                case '2':
                    array_push($arr, $choice);
                    break;
                case '3':
                    array_push($arr, $choice_more);
                    break;
                case '4':
                    array_push($arr, $time);
                    break;
                case '5':
                    array_push($arr, $file);
                    break;
            }
        }
        return $arr;
    }

    /***
     * @return 提交模版
     */
    public function actionSubStencil()
    {
        $post['ConfigStencil'] = Yii::$app->request->get();
        $type = Yii::$app->request->get('type', '');
        $common_info_id = Yii::$app->request->get('common_info_id', '');
        $feature_info_id = Yii::$app->request->get('feature_info_id', '');
        $base_info_id = Yii::$app->request->get('base_info_id', '');
        $supplement_info_id = Yii::$app->request->get('supplement_info_id', '');
        $name = Yii::$app->request->get('name', '');
        if(!$type || !$name || (!$common_info_id && !$feature_info_id && !$base_info_id && !$supplement_info_id))
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = '非法请求';
            return [];
        }
        $model = new ConfigStencil();
        $model->load($post);
        if(!$model->save())
        {
            Yii::$app->response->statusCode = 400;
            Yii::$app->response->statusText = json_encode($model->getErrors(), JSON_UNESCAPED_UNICODE);
            return [];
        }
        return [];
    }

}