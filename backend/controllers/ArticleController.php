<?php
namespace backend\controllers;

use backend\models\ArticleImg;
use Yii;

use backend\models\ArticleClassify;
use common\models\Paging;
use backend\models\Article;
use backend\models\ArticleRate;
use common\models\ImageThumb;
use backend\controllers\BaseController;
use backend\models\UserRegister;
use backend\models\Group;
use backend\models\Menu;
use yii\web\Controller;

class ArticleController extends BaseController
{
    /**
     * 分类管理
     * @author  ge
     * @since   2018-01-11
     */

    public $_imgtype = ['1' => '轮播图','2' => '平台特色','3' => '平台服务','4' => '产品大全','5' => '攻略贷款','6' => '保证服务','7' => '关于我们','8' => '社区论坛'];
    public $_status = ['0' => '隐藏','1' => '显示'];
    public $_pstatus = ['0' => '','1' => '草稿','2' => '发布','3' => '撤回'];

    public function actionClassify()
    {
        $pageHtml = '';
        $page = Yii::$app->request->get('p', 1);
        $data = ArticleClassify::getlist([], '', $page, 15);

        if ($data['totalPage'] > 0) {
            $page_total = $data['totalPage'];
            $pageHtml = Paging::make($page, $page_total, '/user/list?p=');
        }

        return $this->render('classify', [
            'list' => $data['list'],
            'pageHtml' => $pageHtml
        ]);
    }

    /**
     * 添加分类
     * @author  ge
     * @since   2018-01-11
     */

    public function actionAddClassify()
    {
        $model = new ArticleClassify();
        $post = Yii::$app->request->post();
        $model->author = Yii::$app->user->getIdentity()->username;
        $model->name = $post['name'];
        $model->status = $post['status'];
        $model->create_time = date('Y-m-d H:i:s');
        $model->pid = 0;
        if($post['id'] > 0)
        {
            $id = $post['id'];
            $model = ArticleClassify::find()->where(['id' => $id])->one();
            $model->status = $post['status'];
            $model->name = $post['name'];
            $model->update();
            $username = Yii::$app->user->getIdentity()->username;
            $this->addlog($username, '修改了官网文章分类id:'.$id, '/' . Yii::$app->request->getPathInfo());
            return $this->redirect('/article/classify');
        }
        if ($model->save()) {
            $username = Yii::$app->user->getIdentity()->username;
            $this->addlog($username, '添加了官网文章分类名:'.$model->name, '/' . Yii::$app->request->getPathInfo());
            return $this->redirect('/article/classify');
        }
    }

    /**
     * 分类状态修改
     * @author  ge
     * @since   2018-01-11
     */

    public function actionClassifyStatus()
    {
        $id = Yii::$app->request->post('id');
        $model = ArticleClassify::find()->where(['id' => $id])->one();
        $model->status = $model->status == 0 ? 1 : 0;
        $model->update();
        $username = Yii::$app->user->getIdentity()->username;
        $this->addlog($username, '修改了官网文章分类状态，分类名：'.$model->name.'，状态改为'.$this->_status[$model->status], '/' . Yii::$app->request->getPathInfo());
    }

    /**
     * 文章状态修改
     * @author  ge
     * @since   2018-01-11
     */

    public function actionListStatus()
    {
        $id = Yii::$app->request->post('id');
        $model = Article::find()->where(['id' => $id])->one();
        $pre_status = $model->status;
        $model->status = ($model->status == 1 || $model->status == 3) ? 2 : 3;
        if($model->status == 2)
        {
            $model->publish_time = date('Y-m-d H:i:s');
        }
        $model->update();
        $username = Yii::$app->user->getIdentity()->username;
        $this->addlog($username, '修改了官网文章状态，文章名:'.$model->title.'，状态由'. $this->_pstatus[$pre_status] .'改为'.$this->_pstatus[$model->status], '/' . Yii::$app->request->getPathInfo());
    }

    /**
     * 文章删除
     * @author  ge
     * @since   2018-01-11
     */

    public function actionDel()
    {
        $id = Yii::$app->request->post('id');
        $model = Article::find()->where(['id' => $id])->one();
        $username = Yii::$app->user->getIdentity()->username;
        if($model->type == 1)
        {
            $title = '文章';
        }
        else
        {
            $title = '产品';
        }
        $this->addlog($username, '删除了官网文章，'. $title .'名:'.$model->title, '/' . Yii::$app->request->getPathInfo());
        $model->status = 0;
        $model->update();

    }

    /**
     * 内容列表
     */

    public function actionList()
    {
        $pageHtml = '';
        $page     = intval(Yii::$app->request->get('page',1));
        $pageSize = 15;
        $data = Article::getlist('status > 0', 'id desc', $page, $pageSize);
        $search = [];

        if($data['totalPage']>1)
        {
            $se = http_build_query($search);
            $pageHtml = Paging::make($page, $data['totalPage'], '?'.$se.'&page=');
        }

        return $this->render('list', [
            'list' => $data['list'],
            'pageHtml' => $pageHtml
        ]);
    }

    public function actionAddContent()
    {
        $model = new Article();

        if($model->load($post = Yii::$app->request->post()))
        {
            if(!empty($_FILES['head_img']['name']))
            {
                if ($_FILES['head_img']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['head_img'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->head_img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传图片');
                }
            }
            $model->type = 1;
            $model->status = 1;
            $model->create_time = date('Y-m-d H:i:s');
            $model->update_time = date('Y-m-d H:i:s');
            $model->author = Yii::$app->user->getIdentity()->username;

            if($model->save())
            {
                $username = Yii::$app->user->getIdentity()->username;
                $this->addlog($username, '添加了官网文章，文章名:'.$model->title, '/' . Yii::$app->request->getPathInfo());
                return $this->redirect('/article/list');
            }
        }

        return $this->render('add-content',[
                'model' => $model,
            ]
            );
    }

    public function actionEditContent()
    {
        $id = Yii::$app->request->get('id', '');
        $model = Article::findOne($id);
        $pre_title = $model->title;
        if($model->load($post = Yii::$app->request->post()))
        {
            if(!empty($_FILES['head_img']['name']))
            {
                if ($_FILES['head_img']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['head_img'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->head_img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传图片');
                }
            }
            $model->type = 1;
            $model->update_time = date('Y-m-d H:i:s');
            $model->update();
            $username = Yii::$app->user->getIdentity()->username;
            $this->addlog($username, '修改了官网文章，文章名:'.$model->title. ',原文章名为:'.$pre_title, '/' . Yii::$app->request->getPathInfo());
            return $this->redirect('/article/list');

        }

        return $this->render('edit-content',[
                'model' => $model,
            ]
        );
    }

    public function actionAddProduct()
    {
        $model = new Article();
        $model->is_mortgage = 0;
        $model->position = 0;
        if ($model->load($post = Yii::$app->request->post())) {
            if (!empty($_FILES['company_logo']['name'])) {
                if ($_FILES['company_logo']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['company_logo'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->company_logo = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传图片');
                }
            }
            if (!empty($_FILES['product_logo']['name'])) {
                if ($_FILES['product_logo']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['product_logo'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->product_logo = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传图片');
                }
            }
            if (!empty($_FILES['custom1_img']['name'])) {
                if ($_FILES['custom1_img']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['custom1_img'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->custom1_img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传图片');
                }
            }
            if (!empty($_FILES['custom2_img']['name'])) {
                if ($_FILES['custom2_img']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['custom2_img'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->custom2_img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传图片');
                }
            }
            if (!empty($_FILES['custom3_img']['name'])) {
                if ($_FILES['custom3_img']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['custom3_img'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->custom3_img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传图片');
                }
            }
            $model->type = 2;
            $model->status = 1;
            $model->create_time = date('Y-m-d H:i:s');
            $model->update_time = date('Y-m-d H:i:s');
            $model->author = Yii::$app->user->getIdentity()->username;

            if ($model->save())
            {
                $username = Yii::$app->user->getIdentity()->username;
                $this->addlog($username, '添加了官网产品，产品名:'.$model->title, '/' . Yii::$app->request->getPathInfo());
                return $this->redirect('/article/list');
            }

        }
        return $this->render('add-product',[
                'model' => $model,
            ]
        );
    }

    public function actionEditProduct()
    {
        $id = Yii::$app->request->get('id');
        $model = Article::findOne($id);
        $pre_title = $model->title;
        if ($model->load($post = Yii::$app->request->post())) {
            if (!empty($_FILES['company_logo']['name'])) {
                if ($_FILES['company_logo']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['company_logo'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->company_logo = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传图片');
                }
            }
            if (!empty($_FILES['product_logo']['name'])) {
                if ($_FILES['product_logo']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['product_logo'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->product_logo = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传图片');
                }
            }
            if (!empty($_FILES['custom1_img']['name'])) {
                if ($_FILES['custom1_img']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['custom1_img'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->custom1_img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传图片');
                }
            }
            if (!empty($_FILES['custom2_img']['name'])) {
                if ($_FILES['custom2_img']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['custom2_img'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->custom2_img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传图片');
                }
            }
            if (!empty($_FILES['custom3_img']['name'])) {
                if ($_FILES['custom3_img']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['custom3_img'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->custom3_img = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img', '请上传图片');
                }
            }
            $model->type = 2;
            $model->update_time = date('Y-m-d H:i:s');
            if ($model->update())
            {
                $username = Yii::$app->user->getIdentity()->username;
                $this->addlog($username, '修改了官网产品，产品名:'.$model->title. ',原产品名为:'.$pre_title, '/' . Yii::$app->request->getPathInfo());
                return $this->redirect('/article/list');
            }
        }
        return $this->render('edit-product',[
                'model' => $model,
            ]
        );
    }

    public function actionMarket()
    {
        $model = new ArticleImg();
        $lunbo = $model->getInfo(1);
        $feature = $model->getInfo(2);
        $service = $model->getInfo(3);
        $product = $model->getInfo(4);
        $stra = $model->getInfo(5);
        $prom = $model->getInfo(6);
        $about = $model->getInfo(7);
        $forum = $model->getInfo(8);

        return $this->render('market',[
            'lunbo' => $lunbo,
            'feature' => $feature,
            'service' => $service,
            'product' => $product,
            'stra' => $stra,
            'prom' => $prom,
            'about' => $about,
            'forum' => $forum,
        ]);
    }
    public function actionAddMarket()
    {
        $model = new ArticleImg();
        if($model->load($post = Yii::$app->request->post()))
        {
            if(empty($_FILES['img_url']['name']))
            {
                $model->addError('img_url', '请上传图片');
            }
            if ($_FILES['img_url']['error'] == 0) {
                $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                $result = ImageThumb::uploadImage($_FILES['img_url'], $saveFile);
                if ($result['status'] == 1) {
                    $model->img_url = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    $model->type = Yii::$app->request->get('type');
                    $model->save();
                    $username = Yii::$app->user->getIdentity()->username;
                    $this->addlog($username, '添加了网站图片，类型为:'.$this->_imgtype[$model->type], '/' . Yii::$app->request->getPathInfo());
                    return $this->redirect('/article/market');
                }
            }
        }
        return $this->render('add-market',[
            'model' => $model,
        ]);
    }

    public function actionEditMarket()
    {
        $id = Yii::$app->request->get('id');
        $model = ArticleImg::findOne($id);
        if($model->load($post = Yii::$app->request->post()))
        {
            if (!empty($_FILES['img_url']['name'])) {
                if ($_FILES['img_url']['error'] == 0) {
                    $saveFile = Yii::getAlias('@frontend') . '/web/upload/' . date('Y') . '/' . date('m') . '/' . date('d') . '/';
                    $result = ImageThumb::uploadImage($_FILES['img_url'], $saveFile);
                    if ($result['status'] == 1) {
                        $model->img_url = str_replace(Yii::getAlias('@frontend') . '/web/upload', '', $result['imageDir']);
                    }
                } else {
                    $model->addError('img_url', '请上传图片');
                }
            }
            $model->save();
            $username = Yii::$app->user->getIdentity()->username;
            $this->addlog($username, '修改了网站图片，id为' . $id . '类型为:'.$this->_imgtype[$model->type], '/' . Yii::$app->request->getPathInfo());
            return $this->redirect('/article/market');
        }
        return $this->render('edit-market',[
            'model' => $model,
        ]);
    }

    public function actionDelMarket()
    {
        $id = Yii::$app->request->get('id');
        $model = ArticleImg::find()->where(['id' => $id])->one();
        $model->status = 0;
        $model->update();
        $username = Yii::$app->user->getIdentity()->username;
        $this->addlog($username, '删除了网站图片，id为' . $id, '/' . Yii::$app->request->getPathInfo());
        return $this->redirect('/article/market');
    }

    public function actionRate()
    {
        $model = new ArticleRate();
        $data = $model->find()->where(['pid' => 0])->all();
        $tianjin = $model->find()->where(['pid' => 1])->all();
        return $this->render('rate',[
            'data' => $data,
            'tianjin' => $tianjin
        ]);
    }

    public function actionEditRate()
    {
        $post = Yii::$app->request->post();
        $model = ArticleRate::find()->where(['id' => $post['id']])->one();
        $model->name = $post['name'];
        $model->rate = $post['rate'];
        $model->save();
    }

    /**
     * 添加利率
     * @author  ge
     * @since   2018-01-11
     */

    public function actionAddRate()
    {
        $model = new ArticleRate();
        $post = Yii::$app->request->post();
        $model->name = $post['name'];
        $model->rate = $post['rate'];
        $model->pid = $post['pid'];
        if ($model->save()) {
            return $this->redirect('/article/rate');
        }
    }

    /**
     * 删除利率
     * @author  ge
     * @since   2018-01-11
     */

    public function actionDelRate()
    {
        $id = Yii::$app->request->post('id');
        $model = ArticleRate::find()->where(['id' => $id])->one();
        $model->delete();
    }

    public function actionUpload()
    {
        echo 1;die;
    }
}