<?php
namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;


class ProductController extends BaseController
{

    /***
     *   产品模版库  通用表单库列表
     */
    public function actionCommonList()
    {
        return $this->render('common-list');
    }

    /***
     *   产品模版库  添加通用表单库
     */
    public function actionCommonAdd()
    {
        return $this->render('common-add');
    }

    /***
     *   产品模版库  特色表单列表
     */
    public function actionFeatureList()
    {
        return $this->render('feature-list');
    }

    /***
     *   产品模版库  添加特色表单
     */
    public function actionFeatureAdd()
    {
        return $this->render('feature-add');
    }

    /***
     *   产品模版库  基础材料表单
     */
    public function actionBaseList()
    {
        return $this->render('base-list');
    }

    /***
     *   产品模版库  添加基础材料表单
     */
    public function actionBaseAdd()
    {
        return $this->render('base-add');
    }

    /***
     *   产品模版库  补充材料表单列表
     */
    public function actionSupplementList()
    {
        return $this->render('supplement-list');
    }

    /***
     *   产品模版库  添加补充材料表单
     */
    public function actionSupplementAdd()
    {
        return $this->render('supplement-add');
    }

    /***
     *   产品模版库  产品模版分类
     */
    public function actionStyle()
    {
        return $this->render('style');
    }

    /***
     *   产品模版库  添加新模版
     */
    public function actionAddTemplet()
    {
        return $this->render('add-templet');
    }
}