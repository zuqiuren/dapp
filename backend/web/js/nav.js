﻿$(function(){
	$('.sidebar-nav').find('ul').removeClass('in');
	var href = document.location.href;
	$('.sidebar-nav').find('a').each(function(){
		if(href.indexOf($(this).attr('href'))!=-1){
			$(this).parents('ul').addClass('in');
			$(this).addClass('over');
		}
	});
	
	$('.sidebar-nav').find('a').click(function(){
		$('.sidebar-nav').find('a').removeClass('over');
		$(this).addClass('over');
	});
	
	$('.sidebar-nav a.nav-header').click(function(){
		var href=$(this).attr('href');
		$('.sidebar-nav a.nav-header').each(function(){
			if($(this).attr('href')!=href){
				$(this).next().css({'height':'0px'}).removeClass('in');
			}
		});
		
	});
});